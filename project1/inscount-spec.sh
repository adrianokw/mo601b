#!/bin/bash

#PROGRAM_NAMES="400.perlbench,401.bzip2,403.gcc,410.bwaves,416.gamess,429.mcf,433.milc,434.zeusmp,435.gromacs,436.cactusADM,437.leslie3d,444.namd,445.gobmk,447.dealII,450.soplex,453.povray,454.calculix,456.hmmer,458.sjeng,459.GemsFDTD,462.libquantum,464.h264ref,465.tonto,470.lbm,471.omnetpp,473.astar,481.wrf,482.sphinx3,483.xalancbmk"


# Run #0


cd /home/adrianowahl/spec/benchspec/CPU2006/400.perlbench/run/run_base_ref_gcc43-64bit.0001/

# 400.perlbench #0 checkspam
/home/adrianowahl/Desktop/pin-3.0-76991-gcc-linux/pin -t /home/adrianowahl/Desktop/pin-3.0-76991-gcc-linux/source/tools/ManualExamples/obj-intel64/inscount2.so -o /home/adrianowahl/Desktop/inscount-results/400.perlbench-ref-0-checkspam.out -- ./perlbench_base.gcc43-64bit -I./lib checkspam.pl 2500 5 25 11 150 1 1 1 1 > perlbench.ref.checkspam.out 2> perlbench.ref.checkspam.err

# 400.perlbench #0 diffmail
/home/adrianowahl/Desktop/pin-3.0-76991-gcc-linux/pin -t /home/adrianowahl/Desktop/pin-3.0-76991-gcc-linux/source/tools/ManualExamples/obj-intel64/inscount2.so -o /home/adrianowahl/Desktop/inscount-results/400.perlbench-ref-0-diffmail.out -- ./perlbench_base.gcc43-64bit -I./lib diffmail.pl 4 800 10 17 19 300 > perlbench.ref.diffmail.out 2> perlbench.ref.diffmail.err

# 400.perlbench #0 splitmail
/home/adrianowahl/Desktop/pin-3.0-76991-gcc-linux/pin -t /home/adrianowahl/Desktop/pin-3.0-76991-gcc-linux/source/tools/ManualExamples/obj-intel64/inscount2.so -o /home/adrianowahl/Desktop/inscount-results/400.perlbench-ref-0-splitmail.out -- ./perlbench_base.gcc43-64bit -I./lib splitmail.pl 1600 12 26 16 4500 > perlbench.ref.splitmail.out 2> perlbench.ref.splitmail.err

cd /home/adrianowahl/spec/benchspec/CPU2006/401.bzip2/run/run_base_ref_gcc43-64bit.0001/

# 401.bzip2 #0 source
/home/adrianowahl/Desktop/pin-3.0-76991-gcc-linux/pin -t /home/adrianowahl/Desktop/pin-3.0-76991-gcc-linux/source/tools/ManualExamples/obj-intel64/inscount2.so -o /home/adrianowahl/Desktop/inscount-results/401.bzip2-ref-0-source.out -- ./bzip2_base.gcc43-64bit input.source 280 > bzip2.ref.source.out 2> bzip2.ref.source.err

# 401.bzip2 #0 chicken
/home/adrianowahl/Desktop/pin-3.0-76991-gcc-linux/pin -t /home/adrianowahl/Desktop/pin-3.0-76991-gcc-linux/source/tools/ManualExamples/obj-intel64/inscount2.so -o /home/adrianowahl/Desktop/inscount-results/401.bzip2-ref-0-chicken.out -- ./bzip2_base.gcc43-64bit chicken.jpg 30 > bzip2.ref.chicken.out 2> bzip2.ref.chicken.err

# 401.bzip2 #0 liberty
/home/adrianowahl/Desktop/pin-3.0-76991-gcc-linux/pin -t /home/adrianowahl/Desktop/pin-3.0-76991-gcc-linux/source/tools/ManualExamples/obj-intel64/inscount2.so -o /home/adrianowahl/Desktop/inscount-results/401.bzip2-ref-0-liberty.out -- ./bzip2_base.gcc43-64bit liberty.jpg 30 > bzip2.ref.liberty.out 2> bzip2.ref.liberty.err

# 401.bzip2 #0 program
/home/adrianowahl/Desktop/pin-3.0-76991-gcc-linux/pin -t /home/adrianowahl/Desktop/pin-3.0-76991-gcc-linux/source/tools/ManualExamples/obj-intel64/inscount2.so -o /home/adrianowahl/Desktop/inscount-results/401.bzip2-ref-0-program.out -- ./bzip2_base.gcc43-64bit input.program 280 > bzip2.ref.program.out 2> bzip2.ref.program.err

# 401.bzip2 #0 text
/home/adrianowahl/Desktop/pin-3.0-76991-gcc-linux/pin -t /home/adrianowahl/Desktop/pin-3.0-76991-gcc-linux/source/tools/ManualExamples/obj-intel64/inscount2.so -o /home/adrianowahl/Desktop/inscount-results/401.bzip2-ref-0-text.out -- ./bzip2_base.gcc43-64bit text.html 280 > bzip2.ref.text.out 2> bzip2.ref.text.err

# 401.bzip2 #0 combined
/home/adrianowahl/Desktop/pin-3.0-76991-gcc-linux/pin -t /home/adrianowahl/Desktop/pin-3.0-76991-gcc-linux/source/tools/ManualExamples/obj-intel64/inscount2.so -o /home/adrianowahl/Desktop/inscount-results/401.bzip2-ref-0-combined.out -- ./bzip2_base.gcc43-64bit input.combined 200 > bzip2.ref.combined.out 2> bzip2.ref.combined.err

cd /home/adrianowahl/spec/benchspec/CPU2006/403.gcc/run/run_base_ref_gcc43-64bit.0001/

# 403.gcc #0 166
/home/adrianowahl/Desktop/pin-3.0-76991-gcc-linux/pin -t /home/adrianowahl/Desktop/pin-3.0-76991-gcc-linux/source/tools/ManualExamples/obj-intel64/inscount2.so -o /home/adrianowahl/Desktop/inscount-results/403.gcc-ref-0-166.out -- ./gcc_base.gcc43-64bit 166.in -o 166.s > gcc.ref.166.out 2> gcc.ref.166.err

# 403.gcc #0 200
/home/adrianowahl/Desktop/pin-3.0-76991-gcc-linux/pin -t /home/adrianowahl/Desktop/pin-3.0-76991-gcc-linux/source/tools/ManualExamples/obj-intel64/inscount2.so -o /home/adrianowahl/Desktop/inscount-results/403.gcc-ref-0-200.out -- ./gcc_base.gcc43-64bit 200.in -o 200.s > gcc.ref.200.out 2> gcc.ref.200.err

# 403.gcc #0 c-typeck
/home/adrianowahl/Desktop/pin-3.0-76991-gcc-linux/pin -t /home/adrianowahl/Desktop/pin-3.0-76991-gcc-linux/source/tools/ManualExamples/obj-intel64/inscount2.so -o /home/adrianowahl/Desktop/inscount-results/403.gcc-ref-0-c-typeck.out -- ./gcc_base.gcc43-64bit c-typeck.in -o c-typeck.s > gcc.ref.c-typeck.out 2> gcc.ref.c-typeck.err

# 403.gcc #0 cp-decl
/home/adrianowahl/Desktop/pin-3.0-76991-gcc-linux/pin -t /home/adrianowahl/Desktop/pin-3.0-76991-gcc-linux/source/tools/ManualExamples/obj-intel64/inscount2.so -o /home/adrianowahl/Desktop/inscount-results/403.gcc-ref-0-cp-decl.out -- ./gcc_base.gcc43-64bit cp-decl.in -o cp-decl.s > gcc.ref.cp-decl.out 2> gcc.ref.cp-decl.err

# 403.gcc #0 expr
/home/adrianowahl/Desktop/pin-3.0-76991-gcc-linux/pin -t /home/adrianowahl/Desktop/pin-3.0-76991-gcc-linux/source/tools/ManualExamples/obj-intel64/inscount2.so -o /home/adrianowahl/Desktop/inscount-results/403.gcc-ref-0-expr.out -- ./gcc_base.gcc43-64bit expr.in -o expr.s > gcc.ref.expr.out 2> gcc.ref.expr.err

# 403.gcc #0 expr2
/home/adrianowahl/Desktop/pin-3.0-76991-gcc-linux/pin -t /home/adrianowahl/Desktop/pin-3.0-76991-gcc-linux/source/tools/ManualExamples/obj-intel64/inscount2.so -o /home/adrianowahl/Desktop/inscount-results/403.gcc-ref-0-expr2.out -- ./gcc_base.gcc43-64bit expr2.in -o expr2.s > gcc.ref.expr2.out 2> gcc.ref.expr2.err

# 403.gcc #0 g23
/home/adrianowahl/Desktop/pin-3.0-76991-gcc-linux/pin -t /home/adrianowahl/Desktop/pin-3.0-76991-gcc-linux/source/tools/ManualExamples/obj-intel64/inscount2.so -o /home/adrianowahl/Desktop/inscount-results/403.gcc-ref-0-g23.out -- ./gcc_base.gcc43-64bit g23.in -o g23.s > gcc.ref.g23.out 2> gcc.ref.g23.err

# 403.gcc #0 s04
/home/adrianowahl/Desktop/pin-3.0-76991-gcc-linux/pin -t /home/adrianowahl/Desktop/pin-3.0-76991-gcc-linux/source/tools/ManualExamples/obj-intel64/inscount2.so -o /home/adrianowahl/Desktop/inscount-results/403.gcc-ref-0-s04.out -- ./gcc_base.gcc43-64bit s04.in -o s04.s > gcc.ref.s04.out 2> gcc.ref.s04.err

# 403.gcc #0 scilab
/home/adrianowahl/Desktop/pin-3.0-76991-gcc-linux/pin -t /home/adrianowahl/Desktop/pin-3.0-76991-gcc-linux/source/tools/ManualExamples/obj-intel64/inscount2.so -o /home/adrianowahl/Desktop/inscount-results/403.gcc-ref-0-scilab.out -- ./gcc_base.gcc43-64bit scilab.in -o scilab.s > gcc.ref.scilab.out 2> gcc.ref.scilab.err

cd /home/adrianowahl/spec/benchspec/CPU2006/410.bwaves/run/run_base_ref_gcc43-64bit.0001/

# 410.bwaves #0 ref
/home/adrianowahl/Desktop/pin-3.0-76991-gcc-linux/pin -t /home/adrianowahl/Desktop/pin-3.0-76991-gcc-linux/source/tools/ManualExamples/obj-intel64/inscount2.so -o /home/adrianowahl/Desktop/inscount-results/410.bwaves-ref-0.out -- ./bwaves_base.gcc43-64bit > bwaves.ref.out 2> bwaves.ref.err

cd /home/adrianowahl/spec/benchspec/CPU2006/416.gamess/run/run_base_ref_gcc43-64bit.0001/

# 416.gamess #0 cytosine
/home/adrianowahl/Desktop/pin-3.0-76991-gcc-linux/pin -t /home/adrianowahl/Desktop/pin-3.0-76991-gcc-linux/source/tools/ManualExamples/obj-intel64/inscount2.so -o /home/adrianowahl/Desktop/inscount-results/416.gamess-ref-0-cytosine.out -- ./gamess_base.gcc43-64bit < cytosine.2.config > gamess.ref.cytosine.out 2> gamess.ref.cytosine.err

# 416.gamess #0 h2ocu2+
/home/adrianowahl/Desktop/pin-3.0-76991-gcc-linux/pin -t /home/adrianowahl/Desktop/pin-3.0-76991-gcc-linux/source/tools/ManualExamples/obj-intel64/inscount2.so -o /home/adrianowahl/Desktop/inscount-results/416.gamess-ref-0-h2ocu2+.out -- ./gamess_base.gcc43-64bit < h2ocu2+.gradient.config > gamess.ref.h2ocu2+.out 2> gamess.ref.h2ocu2+.err

# 416.gamess #0 triazolium
/home/adrianowahl/Desktop/pin-3.0-76991-gcc-linux/pin -t /home/adrianowahl/Desktop/pin-3.0-76991-gcc-linux/source/tools/ManualExamples/obj-intel64/inscount2.so -o /home/adrianowahl/Desktop/inscount-results/416.gamess-ref-0-triazolium.out -- ./gamess_base.gcc43-64bit < triazolium.config > gamess.ref.triazolium.out 2> gamess.ref.triazolium.err

cd /home/adrianowahl/spec/benchspec/CPU2006/429.mcf/run/run_base_ref_gcc43-64bit.0001/

# 429.mcf #0 ref
/home/adrianowahl/Desktop/pin-3.0-76991-gcc-linux/pin -t /home/adrianowahl/Desktop/pin-3.0-76991-gcc-linux/source/tools/ManualExamples/obj-intel64/inscount2.so -o /home/adrianowahl/Desktop/inscount-results/429.mcf-ref-0.out -- ./mcf_base.gcc43-64bit inp.in > mcf.ref.out 2> mcf.ref.err

cd /home/adrianowahl/spec/benchspec/CPU2006/433.milc/run/run_base_ref_gcc43-64bit.0001/

# 433.milc #0 ref
/home/adrianowahl/Desktop/pin-3.0-76991-gcc-linux/pin -t /home/adrianowahl/Desktop/pin-3.0-76991-gcc-linux/source/tools/ManualExamples/obj-intel64/inscount2.so -o /home/adrianowahl/Desktop/inscount-results/433.milc-ref-0.out -- ./milc_base.gcc43-64bit < su3imp.in > milc.ref.out 2> milc.ref.err

cd /home/adrianowahl/spec/benchspec/CPU2006/434.zeusmp/run/run_base_ref_gcc43-64bit.0001/

# 434.zeusmp #0 ref
/home/adrianowahl/Desktop/pin-3.0-76991-gcc-linux/pin -t /home/adrianowahl/Desktop/pin-3.0-76991-gcc-linux/source/tools/ManualExamples/obj-intel64/inscount2.so -o /home/adrianowahl/Desktop/inscount-results/434.zeusmp-ref-0.out -- ./zeusmp_base.gcc43-64bit > zeusmp.ref.out 2> zeusmp.ref.err

cd /home/adrianowahl/spec/benchspec/CPU2006/435.gromacs/run/run_base_ref_gcc43-64bit.0001/

# 435.gromacs #0 ref
/home/adrianowahl/Desktop/pin-3.0-76991-gcc-linux/pin -t /home/adrianowahl/Desktop/pin-3.0-76991-gcc-linux/source/tools/ManualExamples/obj-intel64/inscount2.so -o /home/adrianowahl/Desktop/inscount-results/435.gromacs-ref-0.out -- ./gromacs_base.gcc43-64bit -silent -deffnm gromacs -nice 0 > gromacs.ref.out 2> gromacs.ref.err

cd /home/adrianowahl/spec/benchspec/CPU2006/436.cactusADM/run/run_base_ref_gcc43-64bit.0001/

# 436.cactusADM #0 ref
/home/adrianowahl/Desktop/pin-3.0-76991-gcc-linux/pin -t /home/adrianowahl/Desktop/pin-3.0-76991-gcc-linux/source/tools/ManualExamples/obj-intel64/inscount2.so -o /home/adrianowahl/Desktop/inscount-results/436.cactusADM-ref-0.out -- ./cactusADM_base.gcc43-64bit benchADM.par > cactusADM.ref.out 2> cactusADM.ref.err

cd /home/adrianowahl/spec/benchspec/CPU2006/437.leslie3d/run/run_base_ref_gcc43-64bit.0001/

# 437.leslie3d #0 ref
/home/adrianowahl/Desktop/pin-3.0-76991-gcc-linux/pin -t /home/adrianowahl/Desktop/pin-3.0-76991-gcc-linux/source/tools/ManualExamples/obj-intel64/inscount2.so -o /home/adrianowahl/Desktop/inscount-results/437.leslie3d-ref-0.out -- ./leslie3d_base.gcc43-64bit < leslie3d.in > leslie3d.ref.out 2> leslie3d.ref.err

cd /home/adrianowahl/spec/benchspec/CPU2006/444.namd/run/run_base_ref_gcc43-64bit.0001/

# 444.namd #0 ref
/home/adrianowahl/Desktop/pin-3.0-76991-gcc-linux/pin -t /home/adrianowahl/Desktop/pin-3.0-76991-gcc-linux/source/tools/ManualExamples/obj-intel64/inscount2.so -o /home/adrianowahl/Desktop/inscount-results/444.namd-ref-0.out -- ./namd_base.gcc43-64bit --input namd.input --iterations 38 --output namd.out > namd.ref.out 2> namd.ref.err





cd /home/adrianowahl/spec/benchspec/CPU2006/445.gobmk/run/run_base_ref_gcc43-64bit.0001/

# 445.gobmk #0 13x13
/home/adrianowahl/Desktop/pin-3.0-76991-gcc-linux/pin -t /home/adrianowahl/Desktop/pin-3.0-76991-gcc-linux/source/tools/ManualExamples/obj-intel64/inscount2.so -o /home/adrianowahl/Desktop/inscount-results/445.gobmk-ref-0.-13x13out -- ./gobmk_base.gcc43-64bit --quiet --mode gtp < 13x13.tst > gobmk.ref.13x13.out 2> gobmk.ref.13x13.err

# 445.gobmk #0 nngs
/home/adrianowahl/Desktop/pin-3.0-76991-gcc-linux/pin -t /home/adrianowahl/Desktop/pin-3.0-76991-gcc-linux/source/tools/ManualExamples/obj-intel64/inscount2.so -o /home/adrianowahl/Desktop/inscount-results/445.gobmk-ref-0-nngs.out -- ./gobmk_base.gcc43-64bit --quiet --mode gtp < nngs.tst > gobmk.reff.nngs.out 2> gobmk.ref.nngs.err

# 445.gobmk #0 score2
/home/adrianowahl/Desktop/pin-3.0-76991-gcc-linux/pin -t /home/adrianowahl/Desktop/pin-3.0-76991-gcc-linux/source/tools/ManualExamples/obj-intel64/inscount2.so -o /home/adrianowahl/Desktop/inscount-results/445.gobmk-ref-0-score2.out -- ./gobmk_base.gcc43-64bit --quiet --mode gtp < score2.tst > gobmk.ref.score2.out 2> gobmk.ref.score2.err

# 445.gobmk #0 trevorc
/home/adrianowahl/Desktop/pin-3.0-76991-gcc-linux/pin -t /home/adrianowahl/Desktop/pin-3.0-76991-gcc-linux/source/tools/ManualExamples/obj-intel64/inscount2.so -o /home/adrianowahl/Desktop/inscount-results/445.gobmk-ref-0-trevorc.out -- ./gobmk_base.gcc43-64bit --quiet --mode gtp < trevorc.tst > gobmk.ref.trevorc.out 2> gobmk.ref.trevorc.err

# 445.gobmk #0 trevord
/home/adrianowahl/Desktop/pin-3.0-76991-gcc-linux/pin -t /home/adrianowahl/Desktop/pin-3.0-76991-gcc-linux/source/tools/ManualExamples/obj-intel64/inscount2.so -o /home/adrianowahl/Desktop/inscount-results/445.gobmk-ref-0-trevord.out -- ./gobmk_base.gcc43-64bit --quiet --mode gtp < trevord.tst > gobmk.ref.trevord.out 2> gobmk.ref.trevord.err


cd /home/adrianowahl/spec/benchspec/CPU2006/447.dealII/run/run_base_ref_gcc43-64bit.0001/

# 447.dealII #0 ref
/home/adrianowahl/Desktop/pin-3.0-76991-gcc-linux/pin -t /home/adrianowahl/Desktop/pin-3.0-76991-gcc-linux/source/tools/ManualExamples/obj-intel64/inscount2.so -o /home/adrianowahl/Desktop/inscount-results/447.dealII-ref-0.out -- ./dealII_base.gcc43-64bit 23 > dealII.ref.out 2> dealII.ref.err


cd /home/adrianowahl/spec/benchspec/CPU2006/450.soplex/run/run_base_ref_gcc43-64bit.0001/

# 450.soplex #0 pds-50
/home/adrianowahl/Desktop/pin-3.0-76991-gcc-linux/pin -t /home/adrianowahl/Desktop/pin-3.0-76991-gcc-linux/source/tools/ManualExamples/obj-intel64/inscount2.so -o /home/adrianowahl/Desktop/inscount-results/450.soplex-ref-0-pds-50.out -- ./soplex_base.gcc43-64bit -s1 -e -m45000 pds-50.mps > soplex.ref.pds-50.out 2> soplex.ref.pds-50.err

# 450.soplex #0 ref
/home/adrianowahl/Desktop/pin-3.0-76991-gcc-linux/pin -t /home/adrianowahl/Desktop/pin-3.0-76991-gcc-linux/source/tools/ManualExamples/obj-intel64/inscount2.so -o /home/adrianowahl/Desktop/inscount-results/450.soplex-ref-0-ref.out -- ./soplex_base.gcc43-64bit -m3500 ref.mps > soplex.ref.ref.out 2> soplex.ref.ref.err


cd /home/adrianowahl/spec/benchspec/CPU2006/453.povray/run/run_base_ref_gcc43-64bit.0001/

# 453.povray #0 ref
/home/adrianowahl/Desktop/pin-3.0-76991-gcc-linux/pin -t /home/adrianowahl/Desktop/pin-3.0-76991-gcc-linux/source/tools/ManualExamples/obj-intel64/inscount2.so -o /home/adrianowahl/Desktop/inscount-results/453.povray-ref-0.out -- ./povray_base.gcc43-64bit SPEC-benchmark-ref.ini > povray.ref.out 2> povray.ref.err


cd /home/adrianowahl/spec/benchspec/CPU2006/454.calculix/run/run_base_ref_gcc43-64bit.0001/

# 454.calculix #0 ref
/home/adrianowahl/Desktop/pin-3.0-76991-gcc-linux/pin -t /home/adrianowahl/Desktop/pin-3.0-76991-gcc-linux/source/tools/ManualExamples/obj-intel64/inscount2.so -o /home/adrianowahl/Desktop/inscount-results/454.calculix-ref-0.out -- ./calculix_base.gcc43-64bit -i hyperviscoplastic > calculix.ref.out 2> calculix.ref.err


cd /home/adrianowahl/spec/benchspec/CPU2006/456.hmmer/run/run_base_ref_gcc43-64bit.0000/

# 456.hmmer #0 nph3
/home/adrianowahl/Desktop/pin-3.0-76991-gcc-linux/pin -t /home/adrianowahl/Desktop/pin-3.0-76991-gcc-linux/source/tools/ManualExamples/obj-intel64/inscount2.so -o /home/adrianowahl/Desktop/inscount-results/456.hmmer-ref-0-nph3.out -- ./hmmer_base.gcc43-64bit nph3.hmm swiss41 > hmmer.ref.nph3.out 2> hmmer.ref.nph3.err

# 456.hmmer #0 retro
/home/adrianowahl/Desktop/pin-3.0-76991-gcc-linux/pin -t /home/adrianowahl/Desktop/pin-3.0-76991-gcc-linux/source/tools/ManualExamples/obj-intel64/inscount2.so -o /home/adrianowahl/Desktop/inscount-results/456.hmmer-ref-0-retro.out -- ./hmmer_base.gcc43-64bit --fixed 0 --mean 500 --num 500000 --sd 350 --seed 0 retro.hmm > hmmer.ref.retro.out 2> hmmer.ref.retro.err


cd /home/adrianowahl/spec/benchspec/CPU2006/458.sjeng/run/run_base_ref_gcc43-64bit.0000/

# 458.sjeng #0 ref
/home/adrianowahl/Desktop/pin-3.0-76991-gcc-linux/pin -t /home/adrianowahl/Desktop/pin-3.0-76991-gcc-linux/source/tools/ManualExamples/obj-intel64/inscount2.so -o /home/adrianowahl/Desktop/inscount-results/458.sjeng-ref-0.out -- ./sjeng_base.gcc43-64bit ref.txt > sjeng.ref.out 2> sjeng.ref.err


cd /home/adrianowahl/spec/benchspec/CPU2006/459.GemsFDTD/run/run_base_ref_gcc43-64bit.0000/

# 459.GemsFDTD #0 ref
/home/adrianowahl/Desktop/pin-3.0-76991-gcc-linux/pin -t /home/adrianowahl/Desktop/pin-3.0-76991-gcc-linux/source/tools/ManualExamples/obj-intel64/inscount2.so -o /home/adrianowahl/Desktop/inscount-results/459.GemsFDTD-ref-0.out -- ./GemsFDTD_base.gcc43-64bit > GemsFDTD.ref.out 2> GemsFDTD.ref.err


cd /home/adrianowahl/spec/benchspec/CPU2006/462.libquantum/run/run_base_ref_gcc43-64bit.0000/

# 462.libquantum #0 ref
/home/adrianowahl/Desktop/pin-3.0-76991-gcc-linux/pin -t /home/adrianowahl/Desktop/pin-3.0-76991-gcc-linux/source/tools/ManualExamples/obj-intel64/inscount2.so -o /home/adrianowahl/Desktop/inscount-results/462.libquantum-ref-0.out -- ./libquantum_base.gcc43-64bit 1397 8 > libquantum.ref.out 2> libquantum.ref.err







cd /home/adrianowahl/spec/benchspec/CPU2006/464.h264ref/run/run_base_ref_gcc43-64bit.0000/

# 464.h264ref #0 foreman_baseline
/home/adrianowahl/Desktop/pin-3.0-76991-gcc-linux/pin -t /home/adrianowahl/Desktop/pin-3.0-76991-gcc-linux/source/tools/ManualExamples/obj-intel64/inscount2.so -o /home/adrianowahl/Desktop/inscount-results/464.h264ref-ref-0-foreman_baseline.out -- ./h264ref_base.gcc43-64bit -d foreman_ref_encoder_baseline.cfg > h264ref.ref.foreman_baseline.out 2> h264ref.ref.foreman_baseline.err

# 464.h264ref #0 foreman_main
/home/adrianowahl/Desktop/pin-3.0-76991-gcc-linux/pin -t /home/adrianowahl/Desktop/pin-3.0-76991-gcc-linux/source/tools/ManualExamples/obj-intel64/inscount2.so -o /home/adrianowahl/Desktop/inscount-results/464.h264ref-ref-0-foreman_main.out -- ./h264ref_base.gcc43-64bit -d foreman_ref_encoder_main.cfg > h264ref.ref.foreman_main.out 2> h264ref.ref.foreman_main.err

# 464.h264ref #0 sss
/home/adrianowahl/Desktop/pin-3.0-76991-gcc-linux/pin -t /home/adrianowahl/Desktop/pin-3.0-76991-gcc-linux/source/tools/ManualExamples/obj-intel64/inscount2.so -o /home/adrianowahl/Desktop/inscount-results/464.h264ref-ref-0-sss.out -- ./h264ref_base.gcc43-64bit -d sss_encoder_main.cfg > h264ref.ref.sss.out 2> h264ref.ref.sss.err


cd /home/adrianowahl/spec/benchspec/CPU2006/465.tonto/run/run_base_ref_gcc43-64bit.0000/

# 465.tonto #0 ref
/home/adrianowahl/Desktop/pin-3.0-76991-gcc-linux/pin -t /home/adrianowahl/Desktop/pin-3.0-76991-gcc-linux/source/tools/ManualExamples/obj-intel64/inscount2.so -o /home/adrianowahl/Desktop/inscount-results/465.tonto-ref-0.out -- ./tonto_base.gcc43-64bit > tonto.ref.out 2> tonto.ref.err


cd /home/adrianowahl/spec/benchspec/CPU2006/470.lbm/run/run_base_ref_gcc43-64bit.0000/

# 470.lbm #0 ref
/home/adrianowahl/Desktop/pin-3.0-76991-gcc-linux/pin -t /home/adrianowahl/Desktop/pin-3.0-76991-gcc-linux/source/tools/ManualExamples/obj-intel64/inscount2.so -o /home/adrianowahl/Desktop/inscount-results/470.lbm-ref-0.out -- ./lbm_base.gcc43-64bit 3000 reference.dat 0 0 100_100_130_ldc.of > lbm.ref.out 2> lbm.ref.err


cd /home/adrianowahl/spec/benchspec/CPU2006/471.omnetpp/run/run_base_ref_gcc43-64bit.0000/

# 471.omnetpp #0 ref
/home/adrianowahl/Desktop/pin-3.0-76991-gcc-linux/pin -t /home/adrianowahl/Desktop/pin-3.0-76991-gcc-linux/source/tools/ManualExamples/obj-intel64/inscount2.so -o /home/adrianowahl/Desktop/inscount-results/471.omnetpp-ref-0.out -- ./omnetpp_base.gcc43-64bit omnetpp.ini > omnetpp.ref.log 2> omnetpp.ref.err


cd /home/adrianowahl/spec/benchspec/CPU2006/473.astar/run/run_base_ref_gcc43-64bit.0001/

# 473.astar #0 BigLakes2048
/home/adrianowahl/Desktop/pin-3.0-76991-gcc-linux/pin -t /home/adrianowahl/Desktop/pin-3.0-76991-gcc-linux/source/tools/ManualExamples/obj-intel64/inscount2.so -o /home/adrianowahl/Desktop/inscount-results/473.astar-ref-0-BigLakes2048.out -- ./astar_base.gcc43-64bit BigLakes2048.cfg > astar.ref.BigLakes2048.out 2> astar.ref.BigLakes2048.err

# 473.astar #0 rivers
/home/adrianowahl/Desktop/pin-3.0-76991-gcc-linux/pin -t /home/adrianowahl/Desktop/pin-3.0-76991-gcc-linux/source/tools/ManualExamples/obj-intel64/inscount2.so -o /home/adrianowahl/Desktop/inscount-results/473.astar-ref-0-rivers.out -- ./astar_base.gcc43-64bit rivers.cfg > astar.ref.rivers.out 2> astar.ref.rivers.err


cd /home/adrianowahl/spec/benchspec/CPU2006/481.wrf/run/run_base_ref_gcc43-64bit.0000/

# 481.wrf #0 ref
/home/adrianowahl/Desktop/pin-3.0-76991-gcc-linux/pin -t /home/adrianowahl/Desktop/pin-3.0-76991-gcc-linux/source/tools/ManualExamples/obj-intel64/inscount2.so -o /home/adrianowahl/Desktop/inscount-results/481.wrf-ref-0.out -- ./wrf_base.gcc43-64bit > wrf.ref.out 2> wrf.ref.err


cd /home/adrianowahl/spec/benchspec/CPU2006/482.sphinx3/run/run_base_ref_gcc43-64bit.0000

# 482.sphinx3 #0 ref
/home/adrianowahl/Desktop/pin-3.0-76991-gcc-linux/pin -t /home/adrianowahl/Desktop/pin-3.0-76991-gcc-linux/source/tools/ManualExamples/obj-intel64/inscount2.so -o /home/adrianowahl/Desktop/inscount-results/482.sphinx3-ref-0.out -- ./sphinx_livepretend_base.gcc43-64bit ctlfile . args.an4 > an4.log 2>> an4.err


cd /home/adrianowahl/spec/benchspec/CPU2006/483.xalancbmk/run/run_base_ref_gcc43-64bit.0000

# 483.xalancbmk #0 ref
/home/adrianowahl/Desktop/pin-3.0-76991-gcc-linux/pin -t /home/adrianowahl/Desktop/pin-3.0-76991-gcc-linux/source/tools/ManualExamples/obj-intel64/inscount2.so -o /home/adrianowahl/Desktop/inscount-results/483.xalancbmk-ref-0.out -- ./Xalan_base.gcc43-64bit -v t5.xml xalanc.xsl > xalancbmk.ref.out 2> xalancbmk.ref.err




# Run #1


cd /home/adrianowahl/spec/benchspec/CPU2006/400.perlbench/run/run_base_ref_gcc43-64bit.0001/

# 400.perlbench #1 checkspam
/home/adrianowahl/Desktop/pin-3.0-76991-gcc-linux/pin -t /home/adrianowahl/Desktop/pin-3.0-76991-gcc-linux/source/tools/ManualExamples/obj-intel64/inscount2.so -o /home/adrianowahl/Desktop/inscount-results/400.perlbench-ref-1-checkspam.out -- ./perlbench_base.gcc43-64bit -I./lib checkspam.pl 2500 5 25 11 150 1 1 1 1 > perlbench.ref.checkspam.out 2> perlbench.ref.checkspam.err

# 400.perlbench #1 diffmail
/home/adrianowahl/Desktop/pin-3.0-76991-gcc-linux/pin -t /home/adrianowahl/Desktop/pin-3.0-76991-gcc-linux/source/tools/ManualExamples/obj-intel64/inscount2.so -o /home/adrianowahl/Desktop/inscount-results/400.perlbench-ref-1-diffmail.out -- ./perlbench_base.gcc43-64bit -I./lib diffmail.pl 4 800 10 17 19 300 > perlbench.ref.diffmail.out 2> perlbench.ref.diffmail.err

# 400.perlbench #1 splitmail
/home/adrianowahl/Desktop/pin-3.0-76991-gcc-linux/pin -t /home/adrianowahl/Desktop/pin-3.0-76991-gcc-linux/source/tools/ManualExamples/obj-intel64/inscount2.so -o /home/adrianowahl/Desktop/inscount-results/400.perlbench-ref-1-splitmail.out -- ./perlbench_base.gcc43-64bit -I./lib splitmail.pl 1600 12 26 16 4500 > perlbench.ref.splitmail.out 2> perlbench.ref.splitmail.err

cd /home/adrianowahl/spec/benchspec/CPU2006/401.bzip2/run/run_base_ref_gcc43-64bit.0001/

# 401.bzip2 #1 source
/home/adrianowahl/Desktop/pin-3.0-76991-gcc-linux/pin -t /home/adrianowahl/Desktop/pin-3.0-76991-gcc-linux/source/tools/ManualExamples/obj-intel64/inscount2.so -o /home/adrianowahl/Desktop/inscount-results/401.bzip2-ref-1-source.out -- ./bzip2_base.gcc43-64bit input.source 280 > bzip2.ref.source.out 2> bzip2.ref.source.err

# 401.bzip2 #1 chicken
/home/adrianowahl/Desktop/pin-3.0-76991-gcc-linux/pin -t /home/adrianowahl/Desktop/pin-3.0-76991-gcc-linux/source/tools/ManualExamples/obj-intel64/inscount2.so -o /home/adrianowahl/Desktop/inscount-results/401.bzip2-ref-1-chicken.out -- ./bzip2_base.gcc43-64bit chicken.jpg 30 > bzip2.ref.chicken.out 2> bzip2.ref.chicken.err

# 401.bzip2 #1 liberty
/home/adrianowahl/Desktop/pin-3.0-76991-gcc-linux/pin -t /home/adrianowahl/Desktop/pin-3.0-76991-gcc-linux/source/tools/ManualExamples/obj-intel64/inscount2.so -o /home/adrianowahl/Desktop/inscount-results/401.bzip2-ref-1-liberty.out -- ./bzip2_base.gcc43-64bit liberty.jpg 30 > bzip2.ref.liberty.out 2> bzip2.ref.liberty.err

# 401.bzip2 #1 program
/home/adrianowahl/Desktop/pin-3.0-76991-gcc-linux/pin -t /home/adrianowahl/Desktop/pin-3.0-76991-gcc-linux/source/tools/ManualExamples/obj-intel64/inscount2.so -o /home/adrianowahl/Desktop/inscount-results/401.bzip2-ref-1-program.out -- ./bzip2_base.gcc43-64bit input.program 280 > bzip2.ref.program.out 2> bzip2.ref.program.err

# 401.bzip2 #1 text
/home/adrianowahl/Desktop/pin-3.0-76991-gcc-linux/pin -t /home/adrianowahl/Desktop/pin-3.0-76991-gcc-linux/source/tools/ManualExamples/obj-intel64/inscount2.so -o /home/adrianowahl/Desktop/inscount-results/401.bzip2-ref-1-text.out -- ./bzip2_base.gcc43-64bit text.html 280 > bzip2.ref.text.out 2> bzip2.ref.text.err

# 401.bzip2 #1 combined
/home/adrianowahl/Desktop/pin-3.0-76991-gcc-linux/pin -t /home/adrianowahl/Desktop/pin-3.0-76991-gcc-linux/source/tools/ManualExamples/obj-intel64/inscount2.so -o /home/adrianowahl/Desktop/inscount-results/401.bzip2-ref-1-combined.out -- ./bzip2_base.gcc43-64bit input.combined 200 > bzip2.ref.combined.out 2> bzip2.ref.combined.err


cd /home/adrianowahl/spec/benchspec/CPU2006/403.gcc/run/run_base_ref_gcc43-64bit.0001/

# 403.gcc #1 166
/home/adrianowahl/Desktop/pin-3.0-76991-gcc-linux/pin -t /home/adrianowahl/Desktop/pin-3.0-76991-gcc-linux/source/tools/ManualExamples/obj-intel64/inscount2.so -o /home/adrianowahl/Desktop/inscount-results/403.gcc-ref-1-166.out -- ./gcc_base.gcc43-64bit 166.in -o 166.s > gcc.ref.166.out 2> gcc.ref.166.err

# 403.gcc #1 200
/home/adrianowahl/Desktop/pin-3.0-76991-gcc-linux/pin -t /home/adrianowahl/Desktop/pin-3.0-76991-gcc-linux/source/tools/ManualExamples/obj-intel64/inscount2.so -o /home/adrianowahl/Desktop/inscount-results/403.gcc-ref-1-200.out -- ./gcc_base.gcc43-64bit 200.in -o 200.s > gcc.ref.200.out 2> gcc.ref.200.err

# 403.gcc #1 c-typeck
/home/adrianowahl/Desktop/pin-3.0-76991-gcc-linux/pin -t /home/adrianowahl/Desktop/pin-3.0-76991-gcc-linux/source/tools/ManualExamples/obj-intel64/inscount2.so -o /home/adrianowahl/Desktop/inscount-results/403.gcc-ref-1-c-typeck.out -- ./gcc_base.gcc43-64bit c-typeck.in -o c-typeck.s > gcc.ref.c-typeck.out 2> gcc.ref.c-typeck.err

# 403.gcc #1 cp-decl
/home/adrianowahl/Desktop/pin-3.0-76991-gcc-linux/pin -t /home/adrianowahl/Desktop/pin-3.0-76991-gcc-linux/source/tools/ManualExamples/obj-intel64/inscount2.so -o /home/adrianowahl/Desktop/inscount-results/403.gcc-ref-1-cp-decl.out -- ./gcc_base.gcc43-64bit cp-decl.in -o cp-decl.s > gcc.ref.cp-decl.out 2> gcc.ref.cp-decl.err

# 403.gcc #1 expr
/home/adrianowahl/Desktop/pin-3.0-76991-gcc-linux/pin -t /home/adrianowahl/Desktop/pin-3.0-76991-gcc-linux/source/tools/ManualExamples/obj-intel64/inscount2.so -o /home/adrianowahl/Desktop/inscount-results/403.gcc-ref-1-expr.out -- ./gcc_base.gcc43-64bit expr.in -o expr.s > gcc.ref.expr.out 2> gcc.ref.expr.err

# 403.gcc #1 expr2
/home/adrianowahl/Desktop/pin-3.0-76991-gcc-linux/pin -t /home/adrianowahl/Desktop/pin-3.0-76991-gcc-linux/source/tools/ManualExamples/obj-intel64/inscount2.so -o /home/adrianowahl/Desktop/inscount-results/403.gcc-ref-1-expr2.out -- ./gcc_base.gcc43-64bit expr2.in -o expr2.s > gcc.ref.expr2.out 2> gcc.ref.expr2.err

# 403.gcc #1 g23
/home/adrianowahl/Desktop/pin-3.0-76991-gcc-linux/pin -t /home/adrianowahl/Desktop/pin-3.0-76991-gcc-linux/source/tools/ManualExamples/obj-intel64/inscount2.so -o /home/adrianowahl/Desktop/inscount-results/403.gcc-ref-1-g23.out -- ./gcc_base.gcc43-64bit g23.in -o g23.s > gcc.ref.g23.out 2> gcc.ref.g23.err

# 403.gcc #1 s04
/home/adrianowahl/Desktop/pin-3.0-76991-gcc-linux/pin -t /home/adrianowahl/Desktop/pin-3.0-76991-gcc-linux/source/tools/ManualExamples/obj-intel64/inscount2.so -o /home/adrianowahl/Desktop/inscount-results/403.gcc-ref-1-s04.out -- ./gcc_base.gcc43-64bit s04.in -o s04.s > gcc.ref.s04.out 2> gcc.ref.s04.err

# 403.gcc #1 scilab
/home/adrianowahl/Desktop/pin-3.0-76991-gcc-linux/pin -t /home/adrianowahl/Desktop/pin-3.0-76991-gcc-linux/source/tools/ManualExamples/obj-intel64/inscount2.so -o /home/adrianowahl/Desktop/inscount-results/403.gcc-ref-1-scilab.out -- ./gcc_base.gcc43-64bit scilab.in -o scilab.s > gcc.ref.scilab.out 2> gcc.ref.scilab.err


cd /home/adrianowahl/spec/benchspec/CPU2006/410.bwaves/run/run_base_ref_gcc43-64bit.0001/

# 410.bwaves #1 ref
/home/adrianowahl/Desktop/pin-3.0-76991-gcc-linux/pin -t /home/adrianowahl/Desktop/pin-3.0-76991-gcc-linux/source/tools/ManualExamples/obj-intel64/inscount2.so -o /home/adrianowahl/Desktop/inscount-results/410.bwaves-ref-1.out -- ./bwaves_base.gcc43-64bit > bwaves.ref.out 2> bwaves.ref.err

cd /home/adrianowahl/spec/benchspec/CPU2006/416.gamess/run/run_base_ref_gcc43-64bit.0001/

# 416.gamess #1 cytosine
/home/adrianowahl/Desktop/pin-3.0-76991-gcc-linux/pin -t /home/adrianowahl/Desktop/pin-3.0-76991-gcc-linux/source/tools/ManualExamples/obj-intel64/inscount2.so -o /home/adrianowahl/Desktop/inscount-results/416.gamess-ref-1-cytosine.out -- ./gamess_base.gcc43-64bit < cytosine.2.config > gamess.ref.cytosine.out 2> gamess.ref.cytosine.err

# 416.gamess #1 h2ocu2+
/home/adrianowahl/Desktop/pin-3.0-76991-gcc-linux/pin -t /home/adrianowahl/Desktop/pin-3.0-76991-gcc-linux/source/tools/ManualExamples/obj-intel64/inscount2.so -o /home/adrianowahl/Desktop/inscount-results/416.gamess-ref-1-h2ocu2+.out -- ./gamess_base.gcc43-64bit < h2ocu2+.gradient.config > gamess.ref.h2ocu2+.out 2> gamess.ref.h2ocu2+.err

# 416.gamess #1 triazolium
/home/adrianowahl/Desktop/pin-3.0-76991-gcc-linux/pin -t /home/adrianowahl/Desktop/pin-3.0-76991-gcc-linux/source/tools/ManualExamples/obj-intel64/inscount2.so -o /home/adrianowahl/Desktop/inscount-results/416.gamess-ref-1-triazolium.out -- ./gamess_base.gcc43-64bit < triazolium.config > gamess.ref.triazolium.out 2> gamess.ref.triazolium.err

cd /home/adrianowahl/spec/benchspec/CPU2006/429.mcf/run/run_base_ref_gcc43-64bit.0001/

# 429.mcf #1 ref
/home/adrianowahl/Desktop/pin-3.0-76991-gcc-linux/pin -t /home/adrianowahl/Desktop/pin-3.0-76991-gcc-linux/source/tools/ManualExamples/obj-intel64/inscount2.so -o /home/adrianowahl/Desktop/inscount-results/429.mcf-ref-1.out -- ./mcf_base.gcc43-64bit inp.in > mcf.ref.out 2> mcf.ref.err

cd /home/adrianowahl/spec/benchspec/CPU2006/433.milc/run/run_base_ref_gcc43-64bit.0001/

# 433.milc #1 ref
/home/adrianowahl/Desktop/pin-3.0-76991-gcc-linux/pin -t /home/adrianowahl/Desktop/pin-3.0-76991-gcc-linux/source/tools/ManualExamples/obj-intel64/inscount2.so -o /home/adrianowahl/Desktop/inscount-results/433.milc-ref-1.out -- ./milc_base.gcc43-64bit < su3imp.in > milc.ref.out 2> milc.ref.err

cd /home/adrianowahl/spec/benchspec/CPU2006/434.zeusmp/run/run_base_ref_gcc43-64bit.0001/

# 434.zeusmp #1 ref
/home/adrianowahl/Desktop/pin-3.0-76991-gcc-linux/pin -t /home/adrianowahl/Desktop/pin-3.0-76991-gcc-linux/source/tools/ManualExamples/obj-intel64/inscount2.so -o /home/adrianowahl/Desktop/inscount-results/434.zeusmp-ref-1.out -- ./zeusmp_base.gcc43-64bit > zeusmp.ref.out 2> zeusmp.ref.err

cd /home/adrianowahl/spec/benchspec/CPU2006/435.gromacs/run/run_base_ref_gcc43-64bit.0001/

# 435.gromacs #1 ref
/home/adrianowahl/Desktop/pin-3.0-76991-gcc-linux/pin -t /home/adrianowahl/Desktop/pin-3.0-76991-gcc-linux/source/tools/ManualExamples/obj-intel64/inscount2.so -o /home/adrianowahl/Desktop/inscount-results/435.gromacs-ref-1.out -- ./gromacs_base.gcc43-64bit -silent -deffnm gromacs -nice 0 > gromacs.ref.out 2> gromacs.ref.err

cd /home/adrianowahl/spec/benchspec/CPU2006/436.cactusADM/run/run_base_ref_gcc43-64bit.0001/

# 436.cactusADM #1 ref
/home/adrianowahl/Desktop/pin-3.0-76991-gcc-linux/pin -t /home/adrianowahl/Desktop/pin-3.0-76991-gcc-linux/source/tools/ManualExamples/obj-intel64/inscount2.so -o /home/adrianowahl/Desktop/inscount-results/436.cactusADM-ref-1.out -- ./cactusADM_base.gcc43-64bit benchADM.par > cactusADM.ref.out 2> cactusADM.ref.err

cd /home/adrianowahl/spec/benchspec/CPU2006/437.leslie3d/run/run_base_ref_gcc43-64bit.0001/

# 437.leslie3d #1 ref
/home/adrianowahl/Desktop/pin-3.0-76991-gcc-linux/pin -t /home/adrianowahl/Desktop/pin-3.0-76991-gcc-linux/source/tools/ManualExamples/obj-intel64/inscount2.so -o /home/adrianowahl/Desktop/inscount-results/437.leslie3d-ref-1.out -- ./leslie3d_base.gcc43-64bit < leslie3d.in > leslie3d.ref.out 2> leslie3d.ref.err

cd /home/adrianowahl/spec/benchspec/CPU2006/444.namd/run/run_base_ref_gcc43-64bit.0001/

# 444.namd #1 ref
/home/adrianowahl/Desktop/pin-3.0-76991-gcc-linux/pin -t /home/adrianowahl/Desktop/pin-3.0-76991-gcc-linux/source/tools/ManualExamples/obj-intel64/inscount2.so -o /home/adrianowahl/Desktop/inscount-results/444.namd-ref-1.out -- ./namd_base.gcc43-64bit --input namd.input --iterations 38 --output namd.out > namd.ref.out 2> namd.ref.err


cd /home/adrianowahl/spec/benchspec/CPU2006/445.gobmk/run/run_base_ref_gcc43-64bit.0001/

# 445.gobmk #1 13x13
/home/adrianowahl/Desktop/pin-3.0-76991-gcc-linux/pin -t /home/adrianowahl/Desktop/pin-3.0-76991-gcc-linux/source/tools/ManualExamples/obj-intel64/inscount2.so -o /home/adrianowahl/Desktop/inscount-results/445.gobmk-ref-1.-13x13out -- ./gobmk_base.gcc43-64bit --quiet --mode gtp < 13x13.tst > gobmk.ref.13x13.out 2> gobmk.ref.13x13.err

# 445.gobmk #1 nngs
/home/adrianowahl/Desktop/pin-3.0-76991-gcc-linux/pin -t /home/adrianowahl/Desktop/pin-3.0-76991-gcc-linux/source/tools/ManualExamples/obj-intel64/inscount2.so -o /home/adrianowahl/Desktop/inscount-results/445.gobmk-ref-1-nngs.out -- ./gobmk_base.gcc43-64bit --quiet --mode gtp < nngs.tst > gobmk.reff.nngs.out 2> gobmk.ref.nngs.err

# 445.gobmk #1 score2
/home/adrianowahl/Desktop/pin-3.0-76991-gcc-linux/pin -t /home/adrianowahl/Desktop/pin-3.0-76991-gcc-linux/source/tools/ManualExamples/obj-intel64/inscount2.so -o /home/adrianowahl/Desktop/inscount-results/445.gobmk-ref-1-score2.out -- ./gobmk_base.gcc43-64bit --quiet --mode gtp < score2.tst > gobmk.ref.score2.out 2> gobmk.ref.score2.err

# 445.gobmk #1 trevorc
/home/adrianowahl/Desktop/pin-3.0-76991-gcc-linux/pin -t /home/adrianowahl/Desktop/pin-3.0-76991-gcc-linux/source/tools/ManualExamples/obj-intel64/inscount2.so -o /home/adrianowahl/Desktop/inscount-results/445.gobmk-ref-1-trevorc.out -- ./gobmk_base.gcc43-64bit --quiet --mode gtp < trevorc.tst > gobmk.ref.trevorc.out 2> gobmk.ref.trevorc.err

# 445.gobmk #1 trevord
/home/adrianowahl/Desktop/pin-3.0-76991-gcc-linux/pin -t /home/adrianowahl/Desktop/pin-3.0-76991-gcc-linux/source/tools/ManualExamples/obj-intel64/inscount2.so -o /home/adrianowahl/Desktop/inscount-results/445.gobmk-ref-1-trevord.out -- ./gobmk_base.gcc43-64bit --quiet --mode gtp < trevord.tst > gobmk.ref.trevord.out 2> gobmk.ref.trevord.err


cd /home/adrianowahl/spec/benchspec/CPU2006/447.dealII/run/run_base_ref_gcc43-64bit.0001/

# 447.dealII #1 ref
/home/adrianowahl/Desktop/pin-3.0-76991-gcc-linux/pin -t /home/adrianowahl/Desktop/pin-3.0-76991-gcc-linux/source/tools/ManualExamples/obj-intel64/inscount2.so -o /home/adrianowahl/Desktop/inscount-results/447.dealII-ref-1.out -- ./dealII_base.gcc43-64bit 23 > dealII.ref.out 2> dealII.ref.err


cd /home/adrianowahl/spec/benchspec/CPU2006/450.soplex/run/run_base_ref_gcc43-64bit.0001/

# 450.soplex #1 pds-50
/home/adrianowahl/Desktop/pin-3.0-76991-gcc-linux/pin -t /home/adrianowahl/Desktop/pin-3.0-76991-gcc-linux/source/tools/ManualExamples/obj-intel64/inscount2.so -o /home/adrianowahl/Desktop/inscount-results/450.soplex-ref-1-pds-50.out -- ./soplex_base.gcc43-64bit -s1 -e -m45000 pds-50.mps > soplex.ref.pds-50.out 2> soplex.ref.pds-50.err

# 450.soplex #1 ref
/home/adrianowahl/Desktop/pin-3.0-76991-gcc-linux/pin -t /home/adrianowahl/Desktop/pin-3.0-76991-gcc-linux/source/tools/ManualExamples/obj-intel64/inscount2.so -o /home/adrianowahl/Desktop/inscount-results/450.soplex-ref-1-ref.out -- ./soplex_base.gcc43-64bit -m3500 ref.mps > soplex.ref.ref.out 2> soplex.ref.ref.err


cd /home/adrianowahl/spec/benchspec/CPU2006/453.povray/run/run_base_ref_gcc43-64bit.0001/

# 453.povray #1 ref
/home/adrianowahl/Desktop/pin-3.0-76991-gcc-linux/pin -t /home/adrianowahl/Desktop/pin-3.0-76991-gcc-linux/source/tools/ManualExamples/obj-intel64/inscount2.so -o /home/adrianowahl/Desktop/inscount-results/453.povray-ref-1.out -- ./povray_base.gcc43-64bit SPEC-benchmark-ref.ini > povray.ref.out 2> povray.ref.err


cd /home/adrianowahl/spec/benchspec/CPU2006/454.calculix/run/run_base_ref_gcc43-64bit.0001/

# 454.calculix #1 ref
/home/adrianowahl/Desktop/pin-3.0-76991-gcc-linux/pin -t /home/adrianowahl/Desktop/pin-3.0-76991-gcc-linux/source/tools/ManualExamples/obj-intel64/inscount2.so -o /home/adrianowahl/Desktop/inscount-results/454.calculix-ref-1.out -- ./calculix_base.gcc43-64bit -i hyperviscoplastic > calculix.ref.out 2> calculix.ref.err


cd /home/adrianowahl/spec/benchspec/CPU2006/456.hmmer/run/run_base_ref_gcc43-64bit.0001/

# 456.hmmer #1 nph3
/home/adrianowahl/Desktop/pin-3.0-76991-gcc-linux/pin -t /home/adrianowahl/Desktop/pin-3.0-76991-gcc-linux/source/tools/ManualExamples/obj-intel64/inscount2.so -o /home/adrianowahl/Desktop/inscount-results/456.hmmer-ref-1-nph3.out -- ./hmmer_base.gcc43-64bit nph3.hmm swiss41 > hmmer.ref.nph3.out 2> hmmer.ref.nph3.err

# 456.hmmer #1 retro
/home/adrianowahl/Desktop/pin-3.0-76991-gcc-linux/pin -t /home/adrianowahl/Desktop/pin-3.0-76991-gcc-linux/source/tools/ManualExamples/obj-intel64/inscount2.so -o /home/adrianowahl/Desktop/inscount-results/456.hmmer-ref-1-retro.out -- ./hmmer_base.gcc43-64bit --fixed 0 --mean 500 --num 500000 --sd 350 --seed 0 retro.hmm > hmmer.ref.retro.out 2> hmmer.ref.retro.err


cd /home/adrianowahl/spec/benchspec/CPU2006/458.sjeng/run/run_base_ref_gcc43-64bit.0001/

# 458.sjeng #1 ref
/home/adrianowahl/Desktop/pin-3.0-76991-gcc-linux/pin -t /home/adrianowahl/Desktop/pin-3.0-76991-gcc-linux/source/tools/ManualExamples/obj-intel64/inscount2.so -o /home/adrianowahl/Desktop/inscount-results/458.sjeng-ref-1.out -- ./sjeng_base.gcc43-64bit ref.txt > sjeng.ref.out 2> sjeng.ref.err


cd /home/adrianowahl/spec/benchspec/CPU2006/459.GemsFDTD/run/run_base_ref_gcc43-64bit.0001/

# 459.GemsFDTD #1 ref
/home/adrianowahl/Desktop/pin-3.0-76991-gcc-linux/pin -t /home/adrianowahl/Desktop/pin-3.0-76991-gcc-linux/source/tools/ManualExamples/obj-intel64/inscount2.so -o /home/adrianowahl/Desktop/inscount-results/459.GemsFDTD-ref-1.out -- ./GemsFDTD_base.gcc43-64bit > GemsFDTD.ref.out 2> GemsFDTD.ref.err


cd /home/adrianowahl/spec/benchspec/CPU2006/462.libquantum/run/run_base_ref_gcc43-64bit.0001/

# 462.libquantum #1 ref
/home/adrianowahl/Desktop/pin-3.0-76991-gcc-linux/pin -t /home/adrianowahl/Desktop/pin-3.0-76991-gcc-linux/source/tools/ManualExamples/obj-intel64/inscount2.so -o /home/adrianowahl/Desktop/inscount-results/462.libquantum-ref-1.out -- ./libquantum_base.gcc43-64bit 1397 8 > libquantum.ref.out 2> libquantum.ref.err


cd /home/adrianowahl/spec/benchspec/CPU2006/464.h264ref/run/run_base_ref_gcc43-64bit.0000/

# 464.h264ref #1 foreman_baseline
/home/adrianowahl/Desktop/pin-3.0-76991-gcc-linux/pin -t /home/adrianowahl/Desktop/pin-3.0-76991-gcc-linux/source/tools/ManualExamples/obj-intel64/inscount2.so -o /home/adrianowahl/Desktop/inscount-results/464.h264ref-ref-1-foreman_baseline.out -- ./h264ref_base.gcc43-64bit -d foreman_ref_encoder_baseline.cfg > h264ref.ref.foreman_baseline.out 2> h264ref.ref.foreman_baseline.err

# 464.h264ref #1 foreman_main
/home/adrianowahl/Desktop/pin-3.0-76991-gcc-linux/pin -t /home/adrianowahl/Desktop/pin-3.0-76991-gcc-linux/source/tools/ManualExamples/obj-intel64/inscount2.so -o /home/adrianowahl/Desktop/inscount-results/464.h264ref-ref-1-foreman_main.out -- ./h264ref_base.gcc43-64bit -d foreman_ref_encoder_main.cfg > h264ref.ref.foreman_main.out 2> h264ref.ref.foreman_main.err

# 464.h264ref #1 sss
/home/adrianowahl/Desktop/pin-3.0-76991-gcc-linux/pin -t /home/adrianowahl/Desktop/pin-3.0-76991-gcc-linux/source/tools/ManualExamples/obj-intel64/inscount2.so -o /home/adrianowahl/Desktop/inscount-results/464.h264ref-ref-1-sss.out -- ./h264ref_base.gcc43-64bit -d sss_encoder_main.cfg > h264ref.ref.sss.out 2> h264ref.ref.sss.err


cd /home/adrianowahl/spec/benchspec/CPU2006/465.tonto/run/run_base_ref_gcc43-64bit.0000/

# 465.tonto #1 ref
/home/adrianowahl/Desktop/pin-3.0-76991-gcc-linux/pin -t /home/adrianowahl/Desktop/pin-3.0-76991-gcc-linux/source/tools/ManualExamples/obj-intel64/inscount2.so -o /home/adrianowahl/Desktop/inscount-results/465.tonto-ref-1.out -- ./tonto_base.gcc43-64bit > tonto.ref.out 2> tonto.ref.err


cd /home/adrianowahl/spec/benchspec/CPU2006/470.lbm/run/run_base_ref_gcc43-64bit.0000/

# 470.lbm #1 ref
/home/adrianowahl/Desktop/pin-3.0-76991-gcc-linux/pin -t /home/adrianowahl/Desktop/pin-3.0-76991-gcc-linux/source/tools/ManualExamples/obj-intel64/inscount2.so -o /home/adrianowahl/Desktop/inscount-results/470.lbm-ref-1.out -- ./lbm_base.gcc43-64bit 3000 reference.dat 0 0 100_100_130_ldc.of > lbm.ref.out 2> lbm.ref.err


cd /home/adrianowahl/spec/benchspec/CPU2006/471.omnetpp/run/run_base_ref_gcc43-64bit.0000/

# 471.omnetpp #1 ref
/home/adrianowahl/Desktop/pin-3.0-76991-gcc-linux/pin -t /home/adrianowahl/Desktop/pin-3.0-76991-gcc-linux/source/tools/ManualExamples/obj-intel64/inscount2.so -o /home/adrianowahl/Desktop/inscount-results/471.omnetpp-ref-1.out -- ./omnetpp_base.gcc43-64bit omnetpp.ini > omnetpp.ref.log 2> omnetpp.ref.err


cd /home/adrianowahl/spec/benchspec/CPU2006/473.astar/run/run_base_ref_gcc43-64bit.0001/

# 473.astar #1 BigLakes2048
/home/adrianowahl/Desktop/pin-3.0-76991-gcc-linux/pin -t /home/adrianowahl/Desktop/pin-3.0-76991-gcc-linux/source/tools/ManualExamples/obj-intel64/inscount2.so -o /home/adrianowahl/Desktop/inscount-results/473.astar-ref-1-BigLakes2048.out -- ./astar_base.gcc43-64bit BigLakes2048.cfg > astar.ref.BigLakes2048.out 2> astar.ref.BigLakes2048.err

# 473.astar #1 rivers
/home/adrianowahl/Desktop/pin-3.0-76991-gcc-linux/pin -t /home/adrianowahl/Desktop/pin-3.0-76991-gcc-linux/source/tools/ManualExamples/obj-intel64/inscount2.so -o /home/adrianowahl/Desktop/inscount-results/473.astar-ref-1-rivers.out -- ./astar_base.gcc43-64bit rivers.cfg > astar.ref.rivers.out 2> astar.ref.rivers.err


cd /home/adrianowahl/spec/benchspec/CPU2006/481.wrf/run/run_base_ref_gcc43-64bit.0000/

# 481.wrf #1 ref
/home/adrianowahl/Desktop/pin-3.0-76991-gcc-linux/pin -t /home/adrianowahl/Desktop/pin-3.0-76991-gcc-linux/source/tools/ManualExamples/obj-intel64/inscount2.so -o /home/adrianowahl/Desktop/inscount-results/481.wrf-ref-1.out -- ./wrf_base.gcc43-64bit > wrf.ref.out 2> wrf.ref.err


cd /home/adrianowahl/spec/benchspec/CPU2006/482.sphinx3/run/run_base_ref_gcc43-64bit.0000

# 482.sphinx3 #1 ref
/home/adrianowahl/Desktop/pin-3.0-76991-gcc-linux/pin -t /home/adrianowahl/Desktop/pin-3.0-76991-gcc-linux/source/tools/ManualExamples/obj-intel64/inscount2.so -o /home/adrianowahl/Desktop/inscount-results/482.sphinx3-ref-1.out -- ./sphinx_livepretend_base.gcc43-64bit ctlfile . args.an4 > an4.log 2>> an4.err


cd /home/adrianowahl/spec/benchspec/CPU2006/483.xalancbmk/run/run_base_ref_gcc43-64bit.0000

# 483.xalancbmk #1 ref
/home/adrianowahl/Desktop/pin-3.0-76991-gcc-linux/pin -t /home/adrianowahl/Desktop/pin-3.0-76991-gcc-linux/source/tools/ManualExamples/obj-intel64/inscount2.so -o /home/adrianowahl/Desktop/inscount-results/483.xalancbmk-ref-1.out -- ./Xalan_base.gcc43-64bit -v t5.xml xalanc.xsl > xalancbmk.ref.out 2> xalancbmk.ref.err








# Run #2


cd /home/adrianowahl/spec/benchspec/CPU2006/400.perlbench/run/run_base_ref_gcc43-64bit.0001/

# 400.perlbench #2 checkspam
/home/adrianowahl/Desktop/pin-3.0-76991-gcc-linux/pin -t /home/adrianowahl/Desktop/pin-3.0-76991-gcc-linux/source/tools/ManualExamples/obj-intel64/inscount2.so -o /home/adrianowahl/Desktop/inscount-results/400.perlbench-ref-2-checkspam.out -- ./perlbench_base.gcc43-64bit -I./lib checkspam.pl 2500 5 25 11 150 1 1 1 1 > perlbench.ref.checkspam.out 2> perlbench.ref.checkspam.err

# 400.perlbench #2 diffmail
/home/adrianowahl/Desktop/pin-3.0-76991-gcc-linux/pin -t /home/adrianowahl/Desktop/pin-3.0-76991-gcc-linux/source/tools/ManualExamples/obj-intel64/inscount2.so -o /home/adrianowahl/Desktop/inscount-results/400.perlbench-ref-2-diffmail.out -- ./perlbench_base.gcc43-64bit -I./lib diffmail.pl 4 800 10 17 19 300 > perlbench.ref.diffmail.out 2> perlbench.ref.diffmail.err

# 400.perlbench #2 splitmail
/home/adrianowahl/Desktop/pin-3.0-76991-gcc-linux/pin -t /home/adrianowahl/Desktop/pin-3.0-76991-gcc-linux/source/tools/ManualExamples/obj-intel64/inscount2.so -o /home/adrianowahl/Desktop/inscount-results/400.perlbench-ref-2-splitmail.out -- ./perlbench_base.gcc43-64bit -I./lib splitmail.pl 1600 12 26 16 4500 > perlbench.ref.splitmail.out 2> perlbench.ref.splitmail.err

cd /home/adrianowahl/spec/benchspec/CPU2006/401.bzip2/run/run_base_ref_gcc43-64bit.0001/

# 401.bzip2 #2 source
/home/adrianowahl/Desktop/pin-3.0-76991-gcc-linux/pin -t /home/adrianowahl/Desktop/pin-3.0-76991-gcc-linux/source/tools/ManualExamples/obj-intel64/inscount2.so -o /home/adrianowahl/Desktop/inscount-results/401.bzip2-ref-2-source.out -- ./bzip2_base.gcc43-64bit input.source 280 > bzip2.ref.source.out 2> bzip2.ref.source.err

# 401.bzip2 #2 chicken
/home/adrianowahl/Desktop/pin-3.0-76991-gcc-linux/pin -t /home/adrianowahl/Desktop/pin-3.0-76991-gcc-linux/source/tools/ManualExamples/obj-intel64/inscount2.so -o /home/adrianowahl/Desktop/inscount-results/401.bzip2-ref-2-chicken.out -- ./bzip2_base.gcc43-64bit chicken.jpg 30 > bzip2.ref.chicken.out 2> bzip2.ref.chicken.err

# 401.bzip2 #2 liberty
/home/adrianowahl/Desktop/pin-3.0-76991-gcc-linux/pin -t /home/adrianowahl/Desktop/pin-3.0-76991-gcc-linux/source/tools/ManualExamples/obj-intel64/inscount2.so -o /home/adrianowahl/Desktop/inscount-results/401.bzip2-ref-2-liberty.out -- ./bzip2_base.gcc43-64bit liberty.jpg 30 > bzip2.ref.liberty.out 2> bzip2.ref.liberty.err

# 401.bzip2 #2 program
/home/adrianowahl/Desktop/pin-3.0-76991-gcc-linux/pin -t /home/adrianowahl/Desktop/pin-3.0-76991-gcc-linux/source/tools/ManualExamples/obj-intel64/inscount2.so -o /home/adrianowahl/Desktop/inscount-results/401.bzip2-ref-2-program.out -- ./bzip2_base.gcc43-64bit input.program 280 > bzip2.ref.program.out 2> bzip2.ref.program.err

# 401.bzip2 #2 text
/home/adrianowahl/Desktop/pin-3.0-76991-gcc-linux/pin -t /home/adrianowahl/Desktop/pin-3.0-76991-gcc-linux/source/tools/ManualExamples/obj-intel64/inscount2.so -o /home/adrianowahl/Desktop/inscount-results/401.bzip2-ref-2-text.out -- ./bzip2_base.gcc43-64bit text.html 280 > bzip2.ref.text.out 2> bzip2.ref.text.err

# 401.bzip2 #2 combined
/home/adrianowahl/Desktop/pin-3.0-76991-gcc-linux/pin -t /home/adrianowahl/Desktop/pin-3.0-76991-gcc-linux/source/tools/ManualExamples/obj-intel64/inscount2.so -o /home/adrianowahl/Desktop/inscount-results/401.bzip2-ref-2-combined.out -- ./bzip2_base.gcc43-64bit input.combined 200 > bzip2.ref.combined.out 2> bzip2.ref.combined.err


cd /home/adrianowahl/spec/benchspec/CPU2006/403.gcc/run/run_base_ref_gcc43-64bit.0001/

# 403.gcc #2 166
/home/adrianowahl/Desktop/pin-3.0-76991-gcc-linux/pin -t /home/adrianowahl/Desktop/pin-3.0-76991-gcc-linux/source/tools/ManualExamples/obj-intel64/inscount2.so -o /home/adrianowahl/Desktop/inscount-results/403.gcc-ref-2-166.out -- ./gcc_base.gcc43-64bit 166.in -o 166.s > gcc.ref.166.out 2> gcc.ref.166.err

# 403.gcc #2 200
/home/adrianowahl/Desktop/pin-3.0-76991-gcc-linux/pin -t /home/adrianowahl/Desktop/pin-3.0-76991-gcc-linux/source/tools/ManualExamples/obj-intel64/inscount2.so -o /home/adrianowahl/Desktop/inscount-results/403.gcc-ref-2-200.out -- ./gcc_base.gcc43-64bit 200.in -o 200.s > gcc.ref.200.out 2> gcc.ref.200.err

# 403.gcc #2 c-typeck
/home/adrianowahl/Desktop/pin-3.0-76991-gcc-linux/pin -t /home/adrianowahl/Desktop/pin-3.0-76991-gcc-linux/source/tools/ManualExamples/obj-intel64/inscount2.so -o /home/adrianowahl/Desktop/inscount-results/403.gcc-ref-2-c-typeck.out -- ./gcc_base.gcc43-64bit c-typeck.in -o c-typeck.s > gcc.ref.c-typeck.out 2> gcc.ref.c-typeck.err

# 403.gcc #2 cp-decl
/home/adrianowahl/Desktop/pin-3.0-76991-gcc-linux/pin -t /home/adrianowahl/Desktop/pin-3.0-76991-gcc-linux/source/tools/ManualExamples/obj-intel64/inscount2.so -o /home/adrianowahl/Desktop/inscount-results/403.gcc-ref-2-cp-decl.out -- ./gcc_base.gcc43-64bit cp-decl.in -o cp-decl.s > gcc.ref.cp-decl.out 2> gcc.ref.cp-decl.err

# 403.gcc #2 expr
/home/adrianowahl/Desktop/pin-3.0-76991-gcc-linux/pin -t /home/adrianowahl/Desktop/pin-3.0-76991-gcc-linux/source/tools/ManualExamples/obj-intel64/inscount2.so -o /home/adrianowahl/Desktop/inscount-results/403.gcc-ref-2-expr.out -- ./gcc_base.gcc43-64bit expr.in -o expr.s > gcc.ref.expr.out 2> gcc.ref.expr.err

# 403.gcc #2 expr2
/home/adrianowahl/Desktop/pin-3.0-76991-gcc-linux/pin -t /home/adrianowahl/Desktop/pin-3.0-76991-gcc-linux/source/tools/ManualExamples/obj-intel64/inscount2.so -o /home/adrianowahl/Desktop/inscount-results/403.gcc-ref-2-expr2.out -- ./gcc_base.gcc43-64bit expr2.in -o expr2.s > gcc.ref.expr2.out 2> gcc.ref.expr2.err

# 403.gcc #2 g23
/home/adrianowahl/Desktop/pin-3.0-76991-gcc-linux/pin -t /home/adrianowahl/Desktop/pin-3.0-76991-gcc-linux/source/tools/ManualExamples/obj-intel64/inscount2.so -o /home/adrianowahl/Desktop/inscount-results/403.gcc-ref-2-g23.out -- ./gcc_base.gcc43-64bit g23.in -o g23.s > gcc.ref.g23.out 2> gcc.ref.g23.err

# 403.gcc #2 s04
/home/adrianowahl/Desktop/pin-3.0-76991-gcc-linux/pin -t /home/adrianowahl/Desktop/pin-3.0-76991-gcc-linux/source/tools/ManualExamples/obj-intel64/inscount2.so -o /home/adrianowahl/Desktop/inscount-results/403.gcc-ref-2-s04.out -- ./gcc_base.gcc43-64bit s04.in -o s04.s > gcc.ref.s04.out 2> gcc.ref.s04.err

# 403.gcc #2 scilab
/home/adrianowahl/Desktop/pin-3.0-76991-gcc-linux/pin -t /home/adrianowahl/Desktop/pin-3.0-76991-gcc-linux/source/tools/ManualExamples/obj-intel64/inscount2.so -o /home/adrianowahl/Desktop/inscount-results/403.gcc-ref-2-scilab.out -- ./gcc_base.gcc43-64bit scilab.in -o scilab.s > gcc.ref.scilab.out 2> gcc.ref.scilab.err

cd /home/adrianowahl/spec/benchspec/CPU2006/410.bwaves/run/run_base_ref_gcc43-64bit.0001/

# 410.bwaves #2 ref
/home/adrianowahl/Desktop/pin-3.0-76991-gcc-linux/pin -t /home/adrianowahl/Desktop/pin-3.0-76991-gcc-linux/source/tools/ManualExamples/obj-intel64/inscount2.so -o /home/adrianowahl/Desktop/inscount-results/410.bwaves-ref-2.out -- ./bwaves_base.gcc43-64bit > bwaves.ref.out 2> bwaves.ref.err

cd /home/adrianowahl/spec/benchspec/CPU2006/416.gamess/run/run_base_ref_gcc43-64bit.0001/

# 416.gamess #2 cytosine
/home/adrianowahl/Desktop/pin-3.0-76991-gcc-linux/pin -t /home/adrianowahl/Desktop/pin-3.0-76991-gcc-linux/source/tools/ManualExamples/obj-intel64/inscount2.so -o /home/adrianowahl/Desktop/inscount-results/416.gamess-ref-2-cytosine.out -- ./gamess_base.gcc43-64bit < cytosine.2.config > gamess.ref.cytosine.out 2> gamess.ref.cytosine.err

# 416.gamess #2 h2ocu2+
/home/adrianowahl/Desktop/pin-3.0-76991-gcc-linux/pin -t /home/adrianowahl/Desktop/pin-3.0-76991-gcc-linux/source/tools/ManualExamples/obj-intel64/inscount2.so -o /home/adrianowahl/Desktop/inscount-results/416.gamess-ref-2-h2ocu2+.out -- ./gamess_base.gcc43-64bit < h2ocu2+.gradient.config > gamess.ref.h2ocu2+.out 2> gamess.ref.h2ocu2+.err

# 416.gamess #2 triazolium
/home/adrianowahl/Desktop/pin-3.0-76991-gcc-linux/pin -t /home/adrianowahl/Desktop/pin-3.0-76991-gcc-linux/source/tools/ManualExamples/obj-intel64/inscount2.so -o /home/adrianowahl/Desktop/inscount-results/416.gamess-ref-2-triazolium.out -- ./gamess_base.gcc43-64bit < triazolium.config > gamess.ref.triazolium.out 2> gamess.ref.triazolium.err

cd /home/adrianowahl/spec/benchspec/CPU2006/429.mcf/run/run_base_ref_gcc43-64bit.0001/

# 429.mcf #2 ref
/home/adrianowahl/Desktop/pin-3.0-76991-gcc-linux/pin -t /home/adrianowahl/Desktop/pin-3.0-76991-gcc-linux/source/tools/ManualExamples/obj-intel64/inscount2.so -o /home/adrianowahl/Desktop/inscount-results/429.mcf-ref-2.out -- ./mcf_base.gcc43-64bit inp.in > mcf.ref.out 2> mcf.ref.err

cd /home/adrianowahl/spec/benchspec/CPU2006/433.milc/run/run_base_ref_gcc43-64bit.0001/

# 433.milc #2 ref
/home/adrianowahl/Desktop/pin-3.0-76991-gcc-linux/pin -t /home/adrianowahl/Desktop/pin-3.0-76991-gcc-linux/source/tools/ManualExamples/obj-intel64/inscount2.so -o /home/adrianowahl/Desktop/inscount-results/433.milc-ref-2.out -- ./milc_base.gcc43-64bit < su3imp.in > milc.ref.out 2> milc.ref.err

cd /home/adrianowahl/spec/benchspec/CPU2006/434.zeusmp/run/run_base_ref_gcc43-64bit.0001/

# 434.zeusmp #2 ref
/home/adrianowahl/Desktop/pin-3.0-76991-gcc-linux/pin -t /home/adrianowahl/Desktop/pin-3.0-76991-gcc-linux/source/tools/ManualExamples/obj-intel64/inscount2.so -o /home/adrianowahl/Desktop/inscount-results/434.zeusmp-ref-2.out -- ./zeusmp_base.gcc43-64bit > zeusmp.ref.out 2> zeusmp.ref.err

cd /home/adrianowahl/spec/benchspec/CPU2006/435.gromacs/run/run_base_ref_gcc43-64bit.0001/

# 435.gromacs #2 ref
/home/adrianowahl/Desktop/pin-3.0-76991-gcc-linux/pin -t /home/adrianowahl/Desktop/pin-3.0-76991-gcc-linux/source/tools/ManualExamples/obj-intel64/inscount2.so -o /home/adrianowahl/Desktop/inscount-results/435.gromacs-ref-2.out -- ./gromacs_base.gcc43-64bit -silent -deffnm gromacs -nice 0 > gromacs.ref.out 2> gromacs.ref.err

cd /home/adrianowahl/spec/benchspec/CPU2006/436.cactusADM/run/run_base_ref_gcc43-64bit.0001/

# 436.cactusADM #2 ref
/home/adrianowahl/Desktop/pin-3.0-76991-gcc-linux/pin -t /home/adrianowahl/Desktop/pin-3.0-76991-gcc-linux/source/tools/ManualExamples/obj-intel64/inscount2.so -o /home/adrianowahl/Desktop/inscount-results/436.cactusADM-ref-2.out -- ./cactusADM_base.gcc43-64bit benchADM.par > cactusADM.ref.out 2> cactusADM.ref.err

cd /home/adrianowahl/spec/benchspec/CPU2006/437.leslie3d/run/run_base_ref_gcc43-64bit.0001/

# 437.leslie3d #2 ref
/home/adrianowahl/Desktop/pin-3.0-76991-gcc-linux/pin -t /home/adrianowahl/Desktop/pin-3.0-76991-gcc-linux/source/tools/ManualExamples/obj-intel64/inscount2.so -o /home/adrianowahl/Desktop/inscount-results/437.leslie3d-ref-2.out -- ./leslie3d_base.gcc43-64bit < leslie3d.in > leslie3d.ref.out 2> leslie3d.ref.err

cd /home/adrianowahl/spec/benchspec/CPU2006/444.namd/run/run_base_ref_gcc43-64bit.0001/

# 444.namd #2 ref
/home/adrianowahl/Desktop/pin-3.0-76991-gcc-linux/pin -t /home/adrianowahl/Desktop/pin-3.0-76991-gcc-linux/source/tools/ManualExamples/obj-intel64/inscount2.so -o /home/adrianowahl/Desktop/inscount-results/444.namd-ref-2.out -- ./namd_base.gcc43-64bit --input namd.input --iterations 38 --output namd.out > namd.ref.out 2> namd.ref.err


cd /home/adrianowahl/spec/benchspec/CPU2006/445.gobmk/run/run_base_ref_gcc43-64bit.0001/

# 445.gobmk #2 13x13
/home/adrianowahl/Desktop/pin-3.0-76991-gcc-linux/pin -t /home/adrianowahl/Desktop/pin-3.0-76991-gcc-linux/source/tools/ManualExamples/obj-intel64/inscount2.so -o /home/adrianowahl/Desktop/inscount-results/445.gobmk-ref-2.-13x13out -- ./gobmk_base.gcc43-64bit --quiet --mode gtp < 13x13.tst > gobmk.ref.13x13.out 2> gobmk.ref.13x13.err

# 445.gobmk #2 nngs
/home/adrianowahl/Desktop/pin-3.0-76991-gcc-linux/pin -t /home/adrianowahl/Desktop/pin-3.0-76991-gcc-linux/source/tools/ManualExamples/obj-intel64/inscount2.so -o /home/adrianowahl/Desktop/inscount-results/445.gobmk-ref-2-nngs.out -- ./gobmk_base.gcc43-64bit --quiet --mode gtp < nngs.tst > gobmk.reff.nngs.out 2> gobmk.ref.nngs.err

# 445.gobmk #2 score2
/home/adrianowahl/Desktop/pin-3.0-76991-gcc-linux/pin -t /home/adrianowahl/Desktop/pin-3.0-76991-gcc-linux/source/tools/ManualExamples/obj-intel64/inscount2.so -o /home/adrianowahl/Desktop/inscount-results/445.gobmk-ref-2-score2.out -- ./gobmk_base.gcc43-64bit --quiet --mode gtp < score2.tst > gobmk.ref.score2.out 2> gobmk.ref.score2.err

# 445.gobmk #2 trevorc
/home/adrianowahl/Desktop/pin-3.0-76991-gcc-linux/pin -t /home/adrianowahl/Desktop/pin-3.0-76991-gcc-linux/source/tools/ManualExamples/obj-intel64/inscount2.so -o /home/adrianowahl/Desktop/inscount-results/445.gobmk-ref-2-trevorc.out -- ./gobmk_base.gcc43-64bit --quiet --mode gtp < trevorc.tst > gobmk.ref.trevorc.out 2> gobmk.ref.trevorc.err

# 445.gobmk #2 trevord
/home/adrianowahl/Desktop/pin-3.0-76991-gcc-linux/pin -t /home/adrianowahl/Desktop/pin-3.0-76991-gcc-linux/source/tools/ManualExamples/obj-intel64/inscount2.so -o /home/adrianowahl/Desktop/inscount-results/445.gobmk-ref-2-trevord.out -- ./gobmk_base.gcc43-64bit --quiet --mode gtp < trevord.tst > gobmk.ref.trevord.out 2> gobmk.ref.trevord.err


cd /home/adrianowahl/spec/benchspec/CPU2006/447.dealII/run/run_base_ref_gcc43-64bit.0001/

# 447.dealII #2 ref
/home/adrianowahl/Desktop/pin-3.0-76991-gcc-linux/pin -t /home/adrianowahl/Desktop/pin-3.0-76991-gcc-linux/source/tools/ManualExamples/obj-intel64/inscount2.so -o /home/adrianowahl/Desktop/inscount-results/447.dealII-ref-2.out -- ./dealII_base.gcc43-64bit 23 > dealII.ref.out 2> dealII.ref.err


cd /home/adrianowahl/spec/benchspec/CPU2006/450.soplex/run/run_base_ref_gcc43-64bit.0001/

# 450.soplex #2 pds-50
/home/adrianowahl/Desktop/pin-3.0-76991-gcc-linux/pin -t /home/adrianowahl/Desktop/pin-3.0-76991-gcc-linux/source/tools/ManualExamples/obj-intel64/inscount2.so -o /home/adrianowahl/Desktop/inscount-results/450.soplex-ref-2-pds-50.out -- ./soplex_base.gcc43-64bit -s1 -e -m45000 pds-50.mps > soplex.ref.pds-50.out 2> soplex.ref.pds-50.err

# 450.soplex #2 ref
/home/adrianowahl/Desktop/pin-3.0-76991-gcc-linux/pin -t /home/adrianowahl/Desktop/pin-3.0-76991-gcc-linux/source/tools/ManualExamples/obj-intel64/inscount2.so -o /home/adrianowahl/Desktop/inscount-results/450.soplex-ref-2-ref.out -- ./soplex_base.gcc43-64bit -m3500 ref.mps > soplex.ref.ref.out 2> soplex.ref.ref.err


cd /home/adrianowahl/spec/benchspec/CPU2006/453.povray/run/run_base_ref_gcc43-64bit.0001/

# 453.povray #2 ref
/home/adrianowahl/Desktop/pin-3.0-76991-gcc-linux/pin -t /home/adrianowahl/Desktop/pin-3.0-76991-gcc-linux/source/tools/ManualExamples/obj-intel64/inscount2.so -o /home/adrianowahl/Desktop/inscount-results/453.povray-ref-2.out -- ./povray_base.gcc43-64bit SPEC-benchmark-ref.ini > povray.ref.out 2> povray.ref.err


cd /home/adrianowahl/spec/benchspec/CPU2006/454.calculix/run/run_base_ref_gcc43-64bit.0001/

# 454.calculix #2 ref
/home/adrianowahl/Desktop/pin-3.0-76991-gcc-linux/pin -t /home/adrianowahl/Desktop/pin-3.0-76991-gcc-linux/source/tools/ManualExamples/obj-intel64/inscount2.so -o /home/adrianowahl/Desktop/inscount-results/454.calculix-ref-2.out -- ./calculix_base.gcc43-64bit -i hyperviscoplastic > calculix.ref.out 2> calculix.ref.err


cd /home/adrianowahl/spec/benchspec/CPU2006/456.hmmer/run/run_base_ref_gcc43-64bit.0001/

# 456.hmmer #2 nph3
/home/adrianowahl/Desktop/pin-3.0-76991-gcc-linux/pin -t /home/adrianowahl/Desktop/pin-3.0-76991-gcc-linux/source/tools/ManualExamples/obj-intel64/inscount2.so -o /home/adrianowahl/Desktop/inscount-results/456.hmmer-ref-2-nph3.out -- ./hmmer_base.gcc43-64bit nph3.hmm swiss41 > hmmer.ref.nph3.out 2> hmmer.ref.nph3.err

# 456.hmmer #2 retro
/home/adrianowahl/Desktop/pin-3.0-76991-gcc-linux/pin -t /home/adrianowahl/Desktop/pin-3.0-76991-gcc-linux/source/tools/ManualExamples/obj-intel64/inscount2.so -o /home/adrianowahl/Desktop/inscount-results/456.hmmer-ref-2-retro.out -- ./hmmer_base.gcc43-64bit --fixed 0 --mean 500 --num 500000 --sd 350 --seed 0 retro.hmm > hmmer.ref.retro.out 2> hmmer.ref.retro.err


cd /home/adrianowahl/spec/benchspec/CPU2006/458.sjeng/run/run_base_ref_gcc43-64bit.0001/

# 458.sjeng #2 ref
/home/adrianowahl/Desktop/pin-3.0-76991-gcc-linux/pin -t /home/adrianowahl/Desktop/pin-3.0-76991-gcc-linux/source/tools/ManualExamples/obj-intel64/inscount2.so -o /home/adrianowahl/Desktop/inscount-results/458.sjeng-ref-2.out -- ./sjeng_base.gcc43-64bit ref.txt > sjeng.ref.out 2> sjeng.ref.err


cd /home/adrianowahl/spec/benchspec/CPU2006/459.GemsFDTD/run/run_base_ref_gcc43-64bit.0001/

# 459.GemsFDTD #2 ref
/home/adrianowahl/Desktop/pin-3.0-76991-gcc-linux/pin -t /home/adrianowahl/Desktop/pin-3.0-76991-gcc-linux/source/tools/ManualExamples/obj-intel64/inscount2.so -o /home/adrianowahl/Desktop/inscount-results/459.GemsFDTD-ref-2.out -- ./GemsFDTD_base.gcc43-64bit > GemsFDTD.ref.out 2> GemsFDTD.ref.err


cd /home/adrianowahl/spec/benchspec/CPU2006/462.libquantum/run/run_base_ref_gcc43-64bit.0001/

# 462.libquantum #2 ref
/home/adrianowahl/Desktop/pin-3.0-76991-gcc-linux/pin -t /home/adrianowahl/Desktop/pin-3.0-76991-gcc-linux/source/tools/ManualExamples/obj-intel64/inscount2.so -o /home/adrianowahl/Desktop/inscount-results/462.libquantum-ref-2.out -- ./libquantum_base.gcc43-64bit 1397 8 > libquantum.ref.out 2> libquantum.ref.err


cd /home/adrianowahl/spec/benchspec/CPU2006/464.h264ref/run/run_base_ref_gcc43-64bit.0000/

# 464.h264ref #2 foreman_baseline
/home/adrianowahl/Desktop/pin-3.0-76991-gcc-linux/pin -t /home/adrianowahl/Desktop/pin-3.0-76991-gcc-linux/source/tools/ManualExamples/obj-intel64/inscount2.so -o /home/adrianowahl/Desktop/inscount-results/464.h264ref-ref-2-foreman_baseline.out -- ./h264ref_base.gcc43-64bit -d foreman_ref_encoder_baseline.cfg > h264ref.ref.foreman_baseline.out 2> h264ref.ref.foreman_baseline.err

# 464.h264ref #2 foreman_main
/home/adrianowahl/Desktop/pin-3.0-76991-gcc-linux/pin -t /home/adrianowahl/Desktop/pin-3.0-76991-gcc-linux/source/tools/ManualExamples/obj-intel64/inscount2.so -o /home/adrianowahl/Desktop/inscount-results/464.h264ref-ref-2-foreman_main.out -- ./h264ref_base.gcc43-64bit -d foreman_ref_encoder_main.cfg > h264ref.ref.foreman_main.out 2> h264ref.ref.foreman_main.err

# 464.h264ref #2 sss
/home/adrianowahl/Desktop/pin-3.0-76991-gcc-linux/pin -t /home/adrianowahl/Desktop/pin-3.0-76991-gcc-linux/source/tools/ManualExamples/obj-intel64/inscount2.so -o /home/adrianowahl/Desktop/inscount-results/464.h264ref-ref-2-sss.out -- ./h264ref_base.gcc43-64bit -d sss_encoder_main.cfg > h264ref.ref.sss.out 2> h264ref.ref.sss.err


cd /home/adrianowahl/spec/benchspec/CPU2006/465.tonto/run/run_base_ref_gcc43-64bit.0000/

# 465.tonto #2 ref
/home/adrianowahl/Desktop/pin-3.0-76991-gcc-linux/pin -t /home/adrianowahl/Desktop/pin-3.0-76991-gcc-linux/source/tools/ManualExamples/obj-intel64/inscount2.so -o /home/adrianowahl/Desktop/inscount-results/465.tonto-ref-2.out -- ./tonto_base.gcc43-64bit > tonto.ref.out 2> tonto.ref.err


cd /home/adrianowahl/spec/benchspec/CPU2006/470.lbm/run/run_base_ref_gcc43-64bit.0000/

# 470.lbm #2 ref
/home/adrianowahl/Desktop/pin-3.0-76991-gcc-linux/pin -t /home/adrianowahl/Desktop/pin-3.0-76991-gcc-linux/source/tools/ManualExamples/obj-intel64/inscount2.so -o /home/adrianowahl/Desktop/inscount-results/470.lbm-ref-2.out -- ./lbm_base.gcc43-64bit 3000 reference.dat 0 0 100_100_130_ldc.of > lbm.ref.out 2> lbm.ref.err


cd /home/adrianowahl/spec/benchspec/CPU2006/471.omnetpp/run/run_base_ref_gcc43-64bit.0000/

# 471.omnetpp #2 ref
/home/adrianowahl/Desktop/pin-3.0-76991-gcc-linux/pin -t /home/adrianowahl/Desktop/pin-3.0-76991-gcc-linux/source/tools/ManualExamples/obj-intel64/inscount2.so -o /home/adrianowahl/Desktop/inscount-results/471.omnetpp-ref-2.out -- ./omnetpp_base.gcc43-64bit omnetpp.ini > omnetpp.ref.log 2> omnetpp.ref.err


cd /home/adrianowahl/spec/benchspec/CPU2006/473.astar/run/run_base_ref_gcc43-64bit.0001/

# 473.astar #2 BigLakes2048
/home/adrianowahl/Desktop/pin-3.0-76991-gcc-linux/pin -t /home/adrianowahl/Desktop/pin-3.0-76991-gcc-linux/source/tools/ManualExamples/obj-intel64/inscount2.so -o /home/adrianowahl/Desktop/inscount-results/473.astar-ref-2-BigLakes2048.out -- ./astar_base.gcc43-64bit BigLakes2048.cfg > astar.ref.BigLakes2048.out 2> astar.ref.BigLakes2048.err

# 473.astar #2 rivers
/home/adrianowahl/Desktop/pin-3.0-76991-gcc-linux/pin -t /home/adrianowahl/Desktop/pin-3.0-76991-gcc-linux/source/tools/ManualExamples/obj-intel64/inscount2.so -o /home/adrianowahl/Desktop/inscount-results/473.astar-ref-2-rivers.out -- ./astar_base.gcc43-64bit rivers.cfg > astar.ref.rivers.out 2> astar.ref.rivers.err


cd /home/adrianowahl/spec/benchspec/CPU2006/481.wrf/run/run_base_ref_gcc43-64bit.0000/

# 481.wrf #2 ref
/home/adrianowahl/Desktop/pin-3.0-76991-gcc-linux/pin -t /home/adrianowahl/Desktop/pin-3.0-76991-gcc-linux/source/tools/ManualExamples/obj-intel64/inscount2.so -o /home/adrianowahl/Desktop/inscount-results/481.wrf-ref-2.out -- ./wrf_base.gcc43-64bit > wrf.ref.out 2> wrf.ref.err


cd /home/adrianowahl/spec/benchspec/CPU2006/482.sphinx3/run/run_base_ref_gcc43-64bit.0000

# 482.sphinx3 #2 ref
/home/adrianowahl/Desktop/pin-3.0-76991-gcc-linux/pin -t /home/adrianowahl/Desktop/pin-3.0-76991-gcc-linux/source/tools/ManualExamples/obj-intel64/inscount2.so -o /home/adrianowahl/Desktop/inscount-results/482.sphinx3-ref-2.out -- ./sphinx_livepretend_base.gcc43-64bit ctlfile . args.an4 > an4.log 2>> an4.err


cd /home/adrianowahl/spec/benchspec/CPU2006/483.xalancbmk/run/run_base_ref_gcc43-64bit.0000

# 483.xalancbmk #2 ref
/home/adrianowahl/Desktop/pin-3.0-76991-gcc-linux/pin -t /home/adrianowahl/Desktop/pin-3.0-76991-gcc-linux/source/tools/ManualExamples/obj-intel64/inscount2.so -o /home/adrianowahl/Desktop/inscount-results/483.xalancbmk-ref-2.out -- ./Xalan_base.gcc43-64bit -v t5.xml xalanc.xsl > xalancbmk.ref.out 2> xalancbmk.ref.err




