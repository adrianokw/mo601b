#include <stdio.h>
#include <sys/time.h>
#include <time.h>

#define MIN_RECURSION_DEPTH 0
#define MAX_RECURSION_DEPTH 128
#define INNER_TIMES_TO_REPEAT 100000
#define OUTER_TIMES_TO_REPEAT 200

void do_computation(int recursion, int recursion_depth);
void do_computation1(int recursion, int recursion_depth);
void do_computation2(int recursion, int recursion_depth);
void do_computation3(int recursion, int recursion_depth);
void do_computation4(int recursion, int recursion_depth);
void do_computation5(int recursion, int recursion_depth);
void do_computation6(int recursion, int recursion_depth);
void do_computation7(int recursion, int recursion_depth);
void do_computation8(int recursion, int recursion_depth);
void do_computation9(int recursion, int recursion_depth);
void do_computation10(int recursion, int recursion_depth);
void do_computation11(int recursion, int recursion_depth);
void do_computation12(int recursion, int recursion_depth);
void do_computation13(int recursion, int recursion_depth);
void do_computation14(int recursion, int recursion_depth);
void do_computation15(int recursion, int recursion_depth);
void do_computation16(int recursion, int recursion_depth);
void do_computation17(int recursion, int recursion_depth);
void do_computation18(int recursion, int recursion_depth);
void do_computation19(int recursion, int recursion_depth);
void do_computation20(int recursion, int recursion_depth);
void do_computation21(int recursion, int recursion_depth);
void do_computation22(int recursion, int recursion_depth);
void do_computation23(int recursion, int recursion_depth);
void do_computation24(int recursion, int recursion_depth);
void do_computation25(int recursion, int recursion_depth);
void do_computation26(int recursion, int recursion_depth);
void do_computation27(int recursion, int recursion_depth);
void do_computation28(int recursion, int recursion_depth);
void do_computation29(int recursion, int recursion_depth);
void do_computation30(int recursion, int recursion_depth);
void do_computation31(int recursion, int recursion_depth);
void do_computation32(int recursion, int recursion_depth);

void do_computation(int recursion, int recursion_depth) {
	if(recursion == recursion_depth) {
		return;
	}

	int r = recursion + 1;
	r = r << 27;
	r = r >> 27;
	if(r == 0) {
		do_computation(recursion + 1, recursion_depth);
	}
	else if(r == 1) {
		do_computation1(recursion + 1, recursion_depth);
	}
	else if(r == 2) {
		do_computation2(recursion + 1, recursion_depth);
	}
	else if(r == 3) {
		do_computation3(recursion + 1, recursion_depth);
	}
	else if(r == 4) {
		do_computation4(recursion + 1, recursion_depth);
	}
	else if(r == 5) {
		do_computation5(recursion + 1, recursion_depth);
	}
	else if(r == 6) {
		do_computation6(recursion + 1, recursion_depth);
	}
	else if(r == 7) {
		do_computation7(recursion + 1, recursion_depth);
	}
	else if(r == 8) {
		do_computation8(recursion + 1, recursion_depth);
	}
	else if(r == 9) {
		do_computation9(recursion + 1, recursion_depth);
	}
	else if(r == 10) {
		do_computation10(recursion + 1, recursion_depth);
	}
	else if(r == 11) {
		do_computation11(recursion + 1, recursion_depth);
	}
	else if(r == 12) {
		do_computation12(recursion + 1, recursion_depth);
	}
	else if(r == 13) {
		do_computation13(recursion + 1, recursion_depth);
	}
	else if(r == 14) {
		do_computation14(recursion + 1, recursion_depth);
	}
	else if(r == 15) {
		do_computation15(recursion + 1, recursion_depth);
	}
	else if(r == 16) {
		do_computation16(recursion + 1, recursion_depth);
	}
	else if(r == 17) {
		do_computation17(recursion + 1, recursion_depth);
	}
	else if(r == 18) {
		do_computation18(recursion + 1, recursion_depth);
	}
	else if(r == 19) {
		do_computation19(recursion + 1, recursion_depth);
	}
	else if(r == 20) {
		do_computation20(recursion + 1, recursion_depth);
	}
	else if(r == 21) {
		do_computation21(recursion + 1, recursion_depth);
	}
	else if(r == 22) {
		do_computation22(recursion + 1, recursion_depth);
	}
	else if(r == 23) {
		do_computation23(recursion + 1, recursion_depth);
	}
	else if(r == 24) {
		do_computation24(recursion + 1, recursion_depth);
	}
	else if(r == 25) {
		do_computation25(recursion + 1, recursion_depth);
	}
	else if(r == 26) {
		do_computation26(recursion + 1, recursion_depth);
	}
	else if(r == 27) {
		do_computation27(recursion + 1, recursion_depth);
	}
	else if(r == 28) {
		do_computation28(recursion + 1, recursion_depth);
	}
	else if(r == 29) {
		do_computation29(recursion + 1, recursion_depth);
	}
	else if(r == 30) {
		do_computation30(recursion + 1, recursion_depth);
	}
	else if(r == 31) {
		do_computation31(recursion + 1, recursion_depth);
	}
}

void do_computation1(int recursion, int recursion_depth) {
	if(recursion == recursion_depth) {
		return;
	}

	int r = recursion + 1;
	r = r << 27;
	r = r >> 27;
	if(r == 0) {
		do_computation(recursion + 1, recursion_depth);
	}
	else if(r == 1) {
		do_computation1(recursion + 1, recursion_depth);
	}
	else if(r == 2) {
		do_computation2(recursion + 1, recursion_depth);
	}
	else if(r == 3) {
		do_computation3(recursion + 1, recursion_depth);
	}
	else if(r == 4) {
		do_computation4(recursion + 1, recursion_depth);
	}
	else if(r == 5) {
		do_computation5(recursion + 1, recursion_depth);
	}
	else if(r == 6) {
		do_computation6(recursion + 1, recursion_depth);
	}
	else if(r == 7) {
		do_computation7(recursion + 1, recursion_depth);
	}
	else if(r == 8) {
		do_computation8(recursion + 1, recursion_depth);
	}
	else if(r == 9) {
		do_computation9(recursion + 1, recursion_depth);
	}
	else if(r == 10) {
		do_computation10(recursion + 1, recursion_depth);
	}
	else if(r == 11) {
		do_computation11(recursion + 1, recursion_depth);
	}
	else if(r == 12) {
		do_computation12(recursion + 1, recursion_depth);
	}
	else if(r == 13) {
		do_computation13(recursion + 1, recursion_depth);
	}
	else if(r == 14) {
		do_computation14(recursion + 1, recursion_depth);
	}
	else if(r == 15) {
		do_computation15(recursion + 1, recursion_depth);
	}
	else if(r == 16) {
		do_computation16(recursion + 1, recursion_depth);
	}
	else if(r == 17) {
		do_computation17(recursion + 1, recursion_depth);
	}
	else if(r == 18) {
		do_computation18(recursion + 1, recursion_depth);
	}
	else if(r == 19) {
		do_computation19(recursion + 1, recursion_depth);
	}
	else if(r == 20) {
		do_computation20(recursion + 1, recursion_depth);
	}
	else if(r == 21) {
		do_computation21(recursion + 1, recursion_depth);
	}
	else if(r == 22) {
		do_computation22(recursion + 1, recursion_depth);
	}
	else if(r == 23) {
		do_computation23(recursion + 1, recursion_depth);
	}
	else if(r == 24) {
		do_computation24(recursion + 1, recursion_depth);
	}
	else if(r == 25) {
		do_computation25(recursion + 1, recursion_depth);
	}
	else if(r == 26) {
		do_computation26(recursion + 1, recursion_depth);
	}
	else if(r == 27) {
		do_computation27(recursion + 1, recursion_depth);
	}
	else if(r == 28) {
		do_computation28(recursion + 1, recursion_depth);
	}
	else if(r == 29) {
		do_computation29(recursion + 1, recursion_depth);
	}
	else if(r == 30) {
		do_computation30(recursion + 1, recursion_depth);
	}
	else if(r == 31) {
		do_computation31(recursion + 1, recursion_depth);
	}
}
void do_computation2(int recursion, int recursion_depth) {
	if(recursion == recursion_depth) {
		return;
	}

	int r = recursion + 1;
	r = r << 27;
	r = r >> 27;
	if(r == 0) {
		do_computation(recursion + 1, recursion_depth);
	}
	else if(r == 1) {
		do_computation1(recursion + 1, recursion_depth);
	}
	else if(r == 2) {
		do_computation2(recursion + 1, recursion_depth);
	}
	else if(r == 3) {
		do_computation3(recursion + 1, recursion_depth);
	}
	else if(r == 4) {
		do_computation4(recursion + 1, recursion_depth);
	}
	else if(r == 5) {
		do_computation5(recursion + 1, recursion_depth);
	}
	else if(r == 6) {
		do_computation6(recursion + 1, recursion_depth);
	}
	else if(r == 7) {
		do_computation7(recursion + 1, recursion_depth);
	}
	else if(r == 8) {
		do_computation8(recursion + 1, recursion_depth);
	}
	else if(r == 9) {
		do_computation9(recursion + 1, recursion_depth);
	}
	else if(r == 10) {
		do_computation10(recursion + 1, recursion_depth);
	}
	else if(r == 11) {
		do_computation11(recursion + 1, recursion_depth);
	}
	else if(r == 12) {
		do_computation12(recursion + 1, recursion_depth);
	}
	else if(r == 13) {
		do_computation13(recursion + 1, recursion_depth);
	}
	else if(r == 14) {
		do_computation14(recursion + 1, recursion_depth);
	}
	else if(r == 15) {
		do_computation15(recursion + 1, recursion_depth);
	}
	else if(r == 16) {
		do_computation16(recursion + 1, recursion_depth);
	}
	else if(r == 17) {
		do_computation17(recursion + 1, recursion_depth);
	}
	else if(r == 18) {
		do_computation18(recursion + 1, recursion_depth);
	}
	else if(r == 19) {
		do_computation19(recursion + 1, recursion_depth);
	}
	else if(r == 20) {
		do_computation20(recursion + 1, recursion_depth);
	}
	else if(r == 21) {
		do_computation21(recursion + 1, recursion_depth);
	}
	else if(r == 22) {
		do_computation22(recursion + 1, recursion_depth);
	}
	else if(r == 23) {
		do_computation23(recursion + 1, recursion_depth);
	}
	else if(r == 24) {
		do_computation24(recursion + 1, recursion_depth);
	}
	else if(r == 25) {
		do_computation25(recursion + 1, recursion_depth);
	}
	else if(r == 26) {
		do_computation26(recursion + 1, recursion_depth);
	}
	else if(r == 27) {
		do_computation27(recursion + 1, recursion_depth);
	}
	else if(r == 28) {
		do_computation28(recursion + 1, recursion_depth);
	}
	else if(r == 29) {
		do_computation29(recursion + 1, recursion_depth);
	}
	else if(r == 30) {
		do_computation30(recursion + 1, recursion_depth);
	}
	else if(r == 31) {
		do_computation31(recursion + 1, recursion_depth);
	}
}
void do_computation3(int recursion, int recursion_depth) {
	if(recursion == recursion_depth) {
		return;
	}

	int r = recursion + 1;
	r = r << 27;
	r = r >> 27;
	if(r == 0) {
		do_computation(recursion + 1, recursion_depth);
	}
	else if(r == 1) {
		do_computation1(recursion + 1, recursion_depth);
	}
	else if(r == 2) {
		do_computation2(recursion + 1, recursion_depth);
	}
	else if(r == 3) {
		do_computation3(recursion + 1, recursion_depth);
	}
	else if(r == 4) {
		do_computation4(recursion + 1, recursion_depth);
	}
	else if(r == 5) {
		do_computation5(recursion + 1, recursion_depth);
	}
	else if(r == 6) {
		do_computation6(recursion + 1, recursion_depth);
	}
	else if(r == 7) {
		do_computation7(recursion + 1, recursion_depth);
	}
	else if(r == 8) {
		do_computation8(recursion + 1, recursion_depth);
	}
	else if(r == 9) {
		do_computation9(recursion + 1, recursion_depth);
	}
	else if(r == 10) {
		do_computation10(recursion + 1, recursion_depth);
	}
	else if(r == 11) {
		do_computation11(recursion + 1, recursion_depth);
	}
	else if(r == 12) {
		do_computation12(recursion + 1, recursion_depth);
	}
	else if(r == 13) {
		do_computation13(recursion + 1, recursion_depth);
	}
	else if(r == 14) {
		do_computation14(recursion + 1, recursion_depth);
	}
	else if(r == 15) {
		do_computation15(recursion + 1, recursion_depth);
	}
	else if(r == 16) {
		do_computation16(recursion + 1, recursion_depth);
	}
	else if(r == 17) {
		do_computation17(recursion + 1, recursion_depth);
	}
	else if(r == 18) {
		do_computation18(recursion + 1, recursion_depth);
	}
	else if(r == 19) {
		do_computation19(recursion + 1, recursion_depth);
	}
	else if(r == 20) {
		do_computation20(recursion + 1, recursion_depth);
	}
	else if(r == 21) {
		do_computation21(recursion + 1, recursion_depth);
	}
	else if(r == 22) {
		do_computation22(recursion + 1, recursion_depth);
	}
	else if(r == 23) {
		do_computation23(recursion + 1, recursion_depth);
	}
	else if(r == 24) {
		do_computation24(recursion + 1, recursion_depth);
	}
	else if(r == 25) {
		do_computation25(recursion + 1, recursion_depth);
	}
	else if(r == 26) {
		do_computation26(recursion + 1, recursion_depth);
	}
	else if(r == 27) {
		do_computation27(recursion + 1, recursion_depth);
	}
	else if(r == 28) {
		do_computation28(recursion + 1, recursion_depth);
	}
	else if(r == 29) {
		do_computation29(recursion + 1, recursion_depth);
	}
	else if(r == 30) {
		do_computation30(recursion + 1, recursion_depth);
	}
	else if(r == 31) {
		do_computation31(recursion + 1, recursion_depth);
	}
}
void do_computation4(int recursion, int recursion_depth) {
	if(recursion == recursion_depth) {
		return;
	}

	int r = recursion + 1;
	r = r << 27;
	r = r >> 27;
	if(r == 0) {
		do_computation(recursion + 1, recursion_depth);
	}
	else if(r == 1) {
		do_computation1(recursion + 1, recursion_depth);
	}
	else if(r == 2) {
		do_computation2(recursion + 1, recursion_depth);
	}
	else if(r == 3) {
		do_computation3(recursion + 1, recursion_depth);
	}
	else if(r == 4) {
		do_computation4(recursion + 1, recursion_depth);
	}
	else if(r == 5) {
		do_computation5(recursion + 1, recursion_depth);
	}
	else if(r == 6) {
		do_computation6(recursion + 1, recursion_depth);
	}
	else if(r == 7) {
		do_computation7(recursion + 1, recursion_depth);
	}
	else if(r == 8) {
		do_computation8(recursion + 1, recursion_depth);
	}
	else if(r == 9) {
		do_computation9(recursion + 1, recursion_depth);
	}
	else if(r == 10) {
		do_computation10(recursion + 1, recursion_depth);
	}
	else if(r == 11) {
		do_computation11(recursion + 1, recursion_depth);
	}
	else if(r == 12) {
		do_computation12(recursion + 1, recursion_depth);
	}
	else if(r == 13) {
		do_computation13(recursion + 1, recursion_depth);
	}
	else if(r == 14) {
		do_computation14(recursion + 1, recursion_depth);
	}
	else if(r == 15) {
		do_computation15(recursion + 1, recursion_depth);
	}
	else if(r == 16) {
		do_computation16(recursion + 1, recursion_depth);
	}
	else if(r == 17) {
		do_computation17(recursion + 1, recursion_depth);
	}
	else if(r == 18) {
		do_computation18(recursion + 1, recursion_depth);
	}
	else if(r == 19) {
		do_computation19(recursion + 1, recursion_depth);
	}
	else if(r == 20) {
		do_computation20(recursion + 1, recursion_depth);
	}
	else if(r == 21) {
		do_computation21(recursion + 1, recursion_depth);
	}
	else if(r == 22) {
		do_computation22(recursion + 1, recursion_depth);
	}
	else if(r == 23) {
		do_computation23(recursion + 1, recursion_depth);
	}
	else if(r == 24) {
		do_computation24(recursion + 1, recursion_depth);
	}
	else if(r == 25) {
		do_computation25(recursion + 1, recursion_depth);
	}
	else if(r == 26) {
		do_computation26(recursion + 1, recursion_depth);
	}
	else if(r == 27) {
		do_computation27(recursion + 1, recursion_depth);
	}
	else if(r == 28) {
		do_computation28(recursion + 1, recursion_depth);
	}
	else if(r == 29) {
		do_computation29(recursion + 1, recursion_depth);
	}
	else if(r == 30) {
		do_computation30(recursion + 1, recursion_depth);
	}
	else if(r == 31) {
		do_computation31(recursion + 1, recursion_depth);
	}
}
void do_computation5(int recursion, int recursion_depth) {
	if(recursion == recursion_depth) {
		return;
	}

	int r = recursion + 1;
	r = r << 27;
	r = r >> 27;
	if(r == 0) {
		do_computation(recursion + 1, recursion_depth);
	}
	else if(r == 1) {
		do_computation1(recursion + 1, recursion_depth);
	}
	else if(r == 2) {
		do_computation2(recursion + 1, recursion_depth);
	}
	else if(r == 3) {
		do_computation3(recursion + 1, recursion_depth);
	}
	else if(r == 4) {
		do_computation4(recursion + 1, recursion_depth);
	}
	else if(r == 5) {
		do_computation5(recursion + 1, recursion_depth);
	}
	else if(r == 6) {
		do_computation6(recursion + 1, recursion_depth);
	}
	else if(r == 7) {
		do_computation7(recursion + 1, recursion_depth);
	}
	else if(r == 8) {
		do_computation8(recursion + 1, recursion_depth);
	}
	else if(r == 9) {
		do_computation9(recursion + 1, recursion_depth);
	}
	else if(r == 10) {
		do_computation10(recursion + 1, recursion_depth);
	}
	else if(r == 11) {
		do_computation11(recursion + 1, recursion_depth);
	}
	else if(r == 12) {
		do_computation12(recursion + 1, recursion_depth);
	}
	else if(r == 13) {
		do_computation13(recursion + 1, recursion_depth);
	}
	else if(r == 14) {
		do_computation14(recursion + 1, recursion_depth);
	}
	else if(r == 15) {
		do_computation15(recursion + 1, recursion_depth);
	}
	else if(r == 16) {
		do_computation16(recursion + 1, recursion_depth);
	}
	else if(r == 17) {
		do_computation17(recursion + 1, recursion_depth);
	}
	else if(r == 18) {
		do_computation18(recursion + 1, recursion_depth);
	}
	else if(r == 19) {
		do_computation19(recursion + 1, recursion_depth);
	}
	else if(r == 20) {
		do_computation20(recursion + 1, recursion_depth);
	}
	else if(r == 21) {
		do_computation21(recursion + 1, recursion_depth);
	}
	else if(r == 22) {
		do_computation22(recursion + 1, recursion_depth);
	}
	else if(r == 23) {
		do_computation23(recursion + 1, recursion_depth);
	}
	else if(r == 24) {
		do_computation24(recursion + 1, recursion_depth);
	}
	else if(r == 25) {
		do_computation25(recursion + 1, recursion_depth);
	}
	else if(r == 26) {
		do_computation26(recursion + 1, recursion_depth);
	}
	else if(r == 27) {
		do_computation27(recursion + 1, recursion_depth);
	}
	else if(r == 28) {
		do_computation28(recursion + 1, recursion_depth);
	}
	else if(r == 29) {
		do_computation29(recursion + 1, recursion_depth);
	}
	else if(r == 30) {
		do_computation30(recursion + 1, recursion_depth);
	}
	else if(r == 31) {
		do_computation31(recursion + 1, recursion_depth);
	}
}
void do_computation6(int recursion, int recursion_depth) {
	if(recursion == recursion_depth) {
		return;
	}

	int r = recursion + 1;
	r = r << 27;
	r = r >> 27;
	if(r == 0) {
		do_computation(recursion + 1, recursion_depth);
	}
	else if(r == 1) {
		do_computation1(recursion + 1, recursion_depth);
	}
	else if(r == 2) {
		do_computation2(recursion + 1, recursion_depth);
	}
	else if(r == 3) {
		do_computation3(recursion + 1, recursion_depth);
	}
	else if(r == 4) {
		do_computation4(recursion + 1, recursion_depth);
	}
	else if(r == 5) {
		do_computation5(recursion + 1, recursion_depth);
	}
	else if(r == 6) {
		do_computation6(recursion + 1, recursion_depth);
	}
	else if(r == 7) {
		do_computation7(recursion + 1, recursion_depth);
	}
	else if(r == 8) {
		do_computation8(recursion + 1, recursion_depth);
	}
	else if(r == 9) {
		do_computation9(recursion + 1, recursion_depth);
	}
	else if(r == 10) {
		do_computation10(recursion + 1, recursion_depth);
	}
	else if(r == 11) {
		do_computation11(recursion + 1, recursion_depth);
	}
	else if(r == 12) {
		do_computation12(recursion + 1, recursion_depth);
	}
	else if(r == 13) {
		do_computation13(recursion + 1, recursion_depth);
	}
	else if(r == 14) {
		do_computation14(recursion + 1, recursion_depth);
	}
	else if(r == 15) {
		do_computation15(recursion + 1, recursion_depth);
	}
	else if(r == 16) {
		do_computation16(recursion + 1, recursion_depth);
	}
	else if(r == 17) {
		do_computation17(recursion + 1, recursion_depth);
	}
	else if(r == 18) {
		do_computation18(recursion + 1, recursion_depth);
	}
	else if(r == 19) {
		do_computation19(recursion + 1, recursion_depth);
	}
	else if(r == 20) {
		do_computation20(recursion + 1, recursion_depth);
	}
	else if(r == 21) {
		do_computation21(recursion + 1, recursion_depth);
	}
	else if(r == 22) {
		do_computation22(recursion + 1, recursion_depth);
	}
	else if(r == 23) {
		do_computation23(recursion + 1, recursion_depth);
	}
	else if(r == 24) {
		do_computation24(recursion + 1, recursion_depth);
	}
	else if(r == 25) {
		do_computation25(recursion + 1, recursion_depth);
	}
	else if(r == 26) {
		do_computation26(recursion + 1, recursion_depth);
	}
	else if(r == 27) {
		do_computation27(recursion + 1, recursion_depth);
	}
	else if(r == 28) {
		do_computation28(recursion + 1, recursion_depth);
	}
	else if(r == 29) {
		do_computation29(recursion + 1, recursion_depth);
	}
	else if(r == 30) {
		do_computation30(recursion + 1, recursion_depth);
	}
	else if(r == 31) {
		do_computation31(recursion + 1, recursion_depth);
	}
}
void do_computation7(int recursion, int recursion_depth) {
	if(recursion == recursion_depth) {
		return;
	}

	int r = recursion + 1;
	r = r << 27;
	r = r >> 27;
	if(r == 0) {
		do_computation(recursion + 1, recursion_depth);
	}
	else if(r == 1) {
		do_computation1(recursion + 1, recursion_depth);
	}
	else if(r == 2) {
		do_computation2(recursion + 1, recursion_depth);
	}
	else if(r == 3) {
		do_computation3(recursion + 1, recursion_depth);
	}
	else if(r == 4) {
		do_computation4(recursion + 1, recursion_depth);
	}
	else if(r == 5) {
		do_computation5(recursion + 1, recursion_depth);
	}
	else if(r == 6) {
		do_computation6(recursion + 1, recursion_depth);
	}
	else if(r == 7) {
		do_computation7(recursion + 1, recursion_depth);
	}
	else if(r == 8) {
		do_computation8(recursion + 1, recursion_depth);
	}
	else if(r == 9) {
		do_computation9(recursion + 1, recursion_depth);
	}
	else if(r == 10) {
		do_computation10(recursion + 1, recursion_depth);
	}
	else if(r == 11) {
		do_computation11(recursion + 1, recursion_depth);
	}
	else if(r == 12) {
		do_computation12(recursion + 1, recursion_depth);
	}
	else if(r == 13) {
		do_computation13(recursion + 1, recursion_depth);
	}
	else if(r == 14) {
		do_computation14(recursion + 1, recursion_depth);
	}
	else if(r == 15) {
		do_computation15(recursion + 1, recursion_depth);
	}
	else if(r == 16) {
		do_computation16(recursion + 1, recursion_depth);
	}
	else if(r == 17) {
		do_computation17(recursion + 1, recursion_depth);
	}
	else if(r == 18) {
		do_computation18(recursion + 1, recursion_depth);
	}
	else if(r == 19) {
		do_computation19(recursion + 1, recursion_depth);
	}
	else if(r == 20) {
		do_computation20(recursion + 1, recursion_depth);
	}
	else if(r == 21) {
		do_computation21(recursion + 1, recursion_depth);
	}
	else if(r == 22) {
		do_computation22(recursion + 1, recursion_depth);
	}
	else if(r == 23) {
		do_computation23(recursion + 1, recursion_depth);
	}
	else if(r == 24) {
		do_computation24(recursion + 1, recursion_depth);
	}
	else if(r == 25) {
		do_computation25(recursion + 1, recursion_depth);
	}
	else if(r == 26) {
		do_computation26(recursion + 1, recursion_depth);
	}
	else if(r == 27) {
		do_computation27(recursion + 1, recursion_depth);
	}
	else if(r == 28) {
		do_computation28(recursion + 1, recursion_depth);
	}
	else if(r == 29) {
		do_computation29(recursion + 1, recursion_depth);
	}
	else if(r == 30) {
		do_computation30(recursion + 1, recursion_depth);
	}
	else if(r == 31) {
		do_computation31(recursion + 1, recursion_depth);
	}
}
void do_computation8(int recursion, int recursion_depth) {
	if(recursion == recursion_depth) {
		return;
	}

	int r = recursion + 1;
	r = r << 27;
	r = r >> 27;
	if(r == 0) {
		do_computation(recursion + 1, recursion_depth);
	}
	else if(r == 1) {
		do_computation1(recursion + 1, recursion_depth);
	}
	else if(r == 2) {
		do_computation2(recursion + 1, recursion_depth);
	}
	else if(r == 3) {
		do_computation3(recursion + 1, recursion_depth);
	}
	else if(r == 4) {
		do_computation4(recursion + 1, recursion_depth);
	}
	else if(r == 5) {
		do_computation5(recursion + 1, recursion_depth);
	}
	else if(r == 6) {
		do_computation6(recursion + 1, recursion_depth);
	}
	else if(r == 7) {
		do_computation7(recursion + 1, recursion_depth);
	}
	else if(r == 8) {
		do_computation8(recursion + 1, recursion_depth);
	}
	else if(r == 9) {
		do_computation9(recursion + 1, recursion_depth);
	}
	else if(r == 10) {
		do_computation10(recursion + 1, recursion_depth);
	}
	else if(r == 11) {
		do_computation11(recursion + 1, recursion_depth);
	}
	else if(r == 12) {
		do_computation12(recursion + 1, recursion_depth);
	}
	else if(r == 13) {
		do_computation13(recursion + 1, recursion_depth);
	}
	else if(r == 14) {
		do_computation14(recursion + 1, recursion_depth);
	}
	else if(r == 15) {
		do_computation15(recursion + 1, recursion_depth);
	}
	else if(r == 16) {
		do_computation16(recursion + 1, recursion_depth);
	}
	else if(r == 17) {
		do_computation17(recursion + 1, recursion_depth);
	}
	else if(r == 18) {
		do_computation18(recursion + 1, recursion_depth);
	}
	else if(r == 19) {
		do_computation19(recursion + 1, recursion_depth);
	}
	else if(r == 20) {
		do_computation20(recursion + 1, recursion_depth);
	}
	else if(r == 21) {
		do_computation21(recursion + 1, recursion_depth);
	}
	else if(r == 22) {
		do_computation22(recursion + 1, recursion_depth);
	}
	else if(r == 23) {
		do_computation23(recursion + 1, recursion_depth);
	}
	else if(r == 24) {
		do_computation24(recursion + 1, recursion_depth);
	}
	else if(r == 25) {
		do_computation25(recursion + 1, recursion_depth);
	}
	else if(r == 26) {
		do_computation26(recursion + 1, recursion_depth);
	}
	else if(r == 27) {
		do_computation27(recursion + 1, recursion_depth);
	}
	else if(r == 28) {
		do_computation28(recursion + 1, recursion_depth);
	}
	else if(r == 29) {
		do_computation29(recursion + 1, recursion_depth);
	}
	else if(r == 30) {
		do_computation30(recursion + 1, recursion_depth);
	}
	else if(r == 31) {
		do_computation31(recursion + 1, recursion_depth);
	}
}
void do_computation9(int recursion, int recursion_depth) {
	if(recursion == recursion_depth) {
		return;
	}

	int r = recursion + 1;
	r = r << 27;
	r = r >> 27;
	if(r == 0) {
		do_computation(recursion + 1, recursion_depth);
	}
	else if(r == 1) {
		do_computation1(recursion + 1, recursion_depth);
	}
	else if(r == 2) {
		do_computation2(recursion + 1, recursion_depth);
	}
	else if(r == 3) {
		do_computation3(recursion + 1, recursion_depth);
	}
	else if(r == 4) {
		do_computation4(recursion + 1, recursion_depth);
	}
	else if(r == 5) {
		do_computation5(recursion + 1, recursion_depth);
	}
	else if(r == 6) {
		do_computation6(recursion + 1, recursion_depth);
	}
	else if(r == 7) {
		do_computation7(recursion + 1, recursion_depth);
	}
	else if(r == 8) {
		do_computation8(recursion + 1, recursion_depth);
	}
	else if(r == 9) {
		do_computation9(recursion + 1, recursion_depth);
	}
	else if(r == 10) {
		do_computation10(recursion + 1, recursion_depth);
	}
	else if(r == 11) {
		do_computation11(recursion + 1, recursion_depth);
	}
	else if(r == 12) {
		do_computation12(recursion + 1, recursion_depth);
	}
	else if(r == 13) {
		do_computation13(recursion + 1, recursion_depth);
	}
	else if(r == 14) {
		do_computation14(recursion + 1, recursion_depth);
	}
	else if(r == 15) {
		do_computation15(recursion + 1, recursion_depth);
	}
	else if(r == 16) {
		do_computation16(recursion + 1, recursion_depth);
	}
	else if(r == 17) {
		do_computation17(recursion + 1, recursion_depth);
	}
	else if(r == 18) {
		do_computation18(recursion + 1, recursion_depth);
	}
	else if(r == 19) {
		do_computation19(recursion + 1, recursion_depth);
	}
	else if(r == 20) {
		do_computation20(recursion + 1, recursion_depth);
	}
	else if(r == 21) {
		do_computation21(recursion + 1, recursion_depth);
	}
	else if(r == 22) {
		do_computation22(recursion + 1, recursion_depth);
	}
	else if(r == 23) {
		do_computation23(recursion + 1, recursion_depth);
	}
	else if(r == 24) {
		do_computation24(recursion + 1, recursion_depth);
	}
	else if(r == 25) {
		do_computation25(recursion + 1, recursion_depth);
	}
	else if(r == 26) {
		do_computation26(recursion + 1, recursion_depth);
	}
	else if(r == 27) {
		do_computation27(recursion + 1, recursion_depth);
	}
	else if(r == 28) {
		do_computation28(recursion + 1, recursion_depth);
	}
	else if(r == 29) {
		do_computation29(recursion + 1, recursion_depth);
	}
	else if(r == 30) {
		do_computation30(recursion + 1, recursion_depth);
	}
	else if(r == 31) {
		do_computation31(recursion + 1, recursion_depth);
	}
}
void do_computation10(int recursion, int recursion_depth) {
	if(recursion == recursion_depth) {
		return;
	}

	int r = recursion + 1;
	r = r << 27;
	r = r >> 27;
	if(r == 0) {
		do_computation(recursion + 1, recursion_depth);
	}
	else if(r == 1) {
		do_computation1(recursion + 1, recursion_depth);
	}
	else if(r == 2) {
		do_computation2(recursion + 1, recursion_depth);
	}
	else if(r == 3) {
		do_computation3(recursion + 1, recursion_depth);
	}
	else if(r == 4) {
		do_computation4(recursion + 1, recursion_depth);
	}
	else if(r == 5) {
		do_computation5(recursion + 1, recursion_depth);
	}
	else if(r == 6) {
		do_computation6(recursion + 1, recursion_depth);
	}
	else if(r == 7) {
		do_computation7(recursion + 1, recursion_depth);
	}
	else if(r == 8) {
		do_computation8(recursion + 1, recursion_depth);
	}
	else if(r == 9) {
		do_computation9(recursion + 1, recursion_depth);
	}
	else if(r == 10) {
		do_computation10(recursion + 1, recursion_depth);
	}
	else if(r == 11) {
		do_computation11(recursion + 1, recursion_depth);
	}
	else if(r == 12) {
		do_computation12(recursion + 1, recursion_depth);
	}
	else if(r == 13) {
		do_computation13(recursion + 1, recursion_depth);
	}
	else if(r == 14) {
		do_computation14(recursion + 1, recursion_depth);
	}
	else if(r == 15) {
		do_computation15(recursion + 1, recursion_depth);
	}
	else if(r == 16) {
		do_computation16(recursion + 1, recursion_depth);
	}
	else if(r == 17) {
		do_computation17(recursion + 1, recursion_depth);
	}
	else if(r == 18) {
		do_computation18(recursion + 1, recursion_depth);
	}
	else if(r == 19) {
		do_computation19(recursion + 1, recursion_depth);
	}
	else if(r == 20) {
		do_computation20(recursion + 1, recursion_depth);
	}
	else if(r == 21) {
		do_computation21(recursion + 1, recursion_depth);
	}
	else if(r == 22) {
		do_computation22(recursion + 1, recursion_depth);
	}
	else if(r == 23) {
		do_computation23(recursion + 1, recursion_depth);
	}
	else if(r == 24) {
		do_computation24(recursion + 1, recursion_depth);
	}
	else if(r == 25) {
		do_computation25(recursion + 1, recursion_depth);
	}
	else if(r == 26) {
		do_computation26(recursion + 1, recursion_depth);
	}
	else if(r == 27) {
		do_computation27(recursion + 1, recursion_depth);
	}
	else if(r == 28) {
		do_computation28(recursion + 1, recursion_depth);
	}
	else if(r == 29) {
		do_computation29(recursion + 1, recursion_depth);
	}
	else if(r == 30) {
		do_computation30(recursion + 1, recursion_depth);
	}
	else if(r == 31) {
		do_computation31(recursion + 1, recursion_depth);
	}
}
void do_computation11(int recursion, int recursion_depth) {
	if(recursion == recursion_depth) {
		return;
	}

	int r = recursion + 1;
	r = r << 27;
	r = r >> 27;
	if(r == 0) {
		do_computation(recursion + 1, recursion_depth);
	}
	else if(r == 1) {
		do_computation1(recursion + 1, recursion_depth);
	}
	else if(r == 2) {
		do_computation2(recursion + 1, recursion_depth);
	}
	else if(r == 3) {
		do_computation3(recursion + 1, recursion_depth);
	}
	else if(r == 4) {
		do_computation4(recursion + 1, recursion_depth);
	}
	else if(r == 5) {
		do_computation5(recursion + 1, recursion_depth);
	}
	else if(r == 6) {
		do_computation6(recursion + 1, recursion_depth);
	}
	else if(r == 7) {
		do_computation7(recursion + 1, recursion_depth);
	}
	else if(r == 8) {
		do_computation8(recursion + 1, recursion_depth);
	}
	else if(r == 9) {
		do_computation9(recursion + 1, recursion_depth);
	}
	else if(r == 10) {
		do_computation10(recursion + 1, recursion_depth);
	}
	else if(r == 11) {
		do_computation11(recursion + 1, recursion_depth);
	}
	else if(r == 12) {
		do_computation12(recursion + 1, recursion_depth);
	}
	else if(r == 13) {
		do_computation13(recursion + 1, recursion_depth);
	}
	else if(r == 14) {
		do_computation14(recursion + 1, recursion_depth);
	}
	else if(r == 15) {
		do_computation15(recursion + 1, recursion_depth);
	}
	else if(r == 16) {
		do_computation16(recursion + 1, recursion_depth);
	}
	else if(r == 17) {
		do_computation17(recursion + 1, recursion_depth);
	}
	else if(r == 18) {
		do_computation18(recursion + 1, recursion_depth);
	}
	else if(r == 19) {
		do_computation19(recursion + 1, recursion_depth);
	}
	else if(r == 20) {
		do_computation20(recursion + 1, recursion_depth);
	}
	else if(r == 21) {
		do_computation21(recursion + 1, recursion_depth);
	}
	else if(r == 22) {
		do_computation22(recursion + 1, recursion_depth);
	}
	else if(r == 23) {
		do_computation23(recursion + 1, recursion_depth);
	}
	else if(r == 24) {
		do_computation24(recursion + 1, recursion_depth);
	}
	else if(r == 25) {
		do_computation25(recursion + 1, recursion_depth);
	}
	else if(r == 26) {
		do_computation26(recursion + 1, recursion_depth);
	}
	else if(r == 27) {
		do_computation27(recursion + 1, recursion_depth);
	}
	else if(r == 28) {
		do_computation28(recursion + 1, recursion_depth);
	}
	else if(r == 29) {
		do_computation29(recursion + 1, recursion_depth);
	}
	else if(r == 30) {
		do_computation30(recursion + 1, recursion_depth);
	}
	else if(r == 31) {
		do_computation31(recursion + 1, recursion_depth);
	}
}
void do_computation12(int recursion, int recursion_depth) {
	if(recursion == recursion_depth) {
		return;
	}

	int r = recursion + 1;
	r = r << 27;
	r = r >> 27;
	if(r == 0) {
		do_computation(recursion + 1, recursion_depth);
	}
	else if(r == 1) {
		do_computation1(recursion + 1, recursion_depth);
	}
	else if(r == 2) {
		do_computation2(recursion + 1, recursion_depth);
	}
	else if(r == 3) {
		do_computation3(recursion + 1, recursion_depth);
	}
	else if(r == 4) {
		do_computation4(recursion + 1, recursion_depth);
	}
	else if(r == 5) {
		do_computation5(recursion + 1, recursion_depth);
	}
	else if(r == 6) {
		do_computation6(recursion + 1, recursion_depth);
	}
	else if(r == 7) {
		do_computation7(recursion + 1, recursion_depth);
	}
	else if(r == 8) {
		do_computation8(recursion + 1, recursion_depth);
	}
	else if(r == 9) {
		do_computation9(recursion + 1, recursion_depth);
	}
	else if(r == 10) {
		do_computation10(recursion + 1, recursion_depth);
	}
	else if(r == 11) {
		do_computation11(recursion + 1, recursion_depth);
	}
	else if(r == 12) {
		do_computation12(recursion + 1, recursion_depth);
	}
	else if(r == 13) {
		do_computation13(recursion + 1, recursion_depth);
	}
	else if(r == 14) {
		do_computation14(recursion + 1, recursion_depth);
	}
	else if(r == 15) {
		do_computation15(recursion + 1, recursion_depth);
	}
	else if(r == 16) {
		do_computation16(recursion + 1, recursion_depth);
	}
	else if(r == 17) {
		do_computation17(recursion + 1, recursion_depth);
	}
	else if(r == 18) {
		do_computation18(recursion + 1, recursion_depth);
	}
	else if(r == 19) {
		do_computation19(recursion + 1, recursion_depth);
	}
	else if(r == 20) {
		do_computation20(recursion + 1, recursion_depth);
	}
	else if(r == 21) {
		do_computation21(recursion + 1, recursion_depth);
	}
	else if(r == 22) {
		do_computation22(recursion + 1, recursion_depth);
	}
	else if(r == 23) {
		do_computation23(recursion + 1, recursion_depth);
	}
	else if(r == 24) {
		do_computation24(recursion + 1, recursion_depth);
	}
	else if(r == 25) {
		do_computation25(recursion + 1, recursion_depth);
	}
	else if(r == 26) {
		do_computation26(recursion + 1, recursion_depth);
	}
	else if(r == 27) {
		do_computation27(recursion + 1, recursion_depth);
	}
	else if(r == 28) {
		do_computation28(recursion + 1, recursion_depth);
	}
	else if(r == 29) {
		do_computation29(recursion + 1, recursion_depth);
	}
	else if(r == 30) {
		do_computation30(recursion + 1, recursion_depth);
	}
	else if(r == 31) {
		do_computation31(recursion + 1, recursion_depth);
	}
}
void do_computation13(int recursion, int recursion_depth) {
	if(recursion == recursion_depth) {
		return;
	}

	int r = recursion + 1;
	r = r << 27;
	r = r >> 27;
	if(r == 0) {
		do_computation(recursion + 1, recursion_depth);
	}
	else if(r == 1) {
		do_computation1(recursion + 1, recursion_depth);
	}
	else if(r == 2) {
		do_computation2(recursion + 1, recursion_depth);
	}
	else if(r == 3) {
		do_computation3(recursion + 1, recursion_depth);
	}
	else if(r == 4) {
		do_computation4(recursion + 1, recursion_depth);
	}
	else if(r == 5) {
		do_computation5(recursion + 1, recursion_depth);
	}
	else if(r == 6) {
		do_computation6(recursion + 1, recursion_depth);
	}
	else if(r == 7) {
		do_computation7(recursion + 1, recursion_depth);
	}
	else if(r == 8) {
		do_computation8(recursion + 1, recursion_depth);
	}
	else if(r == 9) {
		do_computation9(recursion + 1, recursion_depth);
	}
	else if(r == 10) {
		do_computation10(recursion + 1, recursion_depth);
	}
	else if(r == 11) {
		do_computation11(recursion + 1, recursion_depth);
	}
	else if(r == 12) {
		do_computation12(recursion + 1, recursion_depth);
	}
	else if(r == 13) {
		do_computation13(recursion + 1, recursion_depth);
	}
	else if(r == 14) {
		do_computation14(recursion + 1, recursion_depth);
	}
	else if(r == 15) {
		do_computation15(recursion + 1, recursion_depth);
	}
	else if(r == 16) {
		do_computation16(recursion + 1, recursion_depth);
	}
	else if(r == 17) {
		do_computation17(recursion + 1, recursion_depth);
	}
	else if(r == 18) {
		do_computation18(recursion + 1, recursion_depth);
	}
	else if(r == 19) {
		do_computation19(recursion + 1, recursion_depth);
	}
	else if(r == 20) {
		do_computation20(recursion + 1, recursion_depth);
	}
	else if(r == 21) {
		do_computation21(recursion + 1, recursion_depth);
	}
	else if(r == 22) {
		do_computation22(recursion + 1, recursion_depth);
	}
	else if(r == 23) {
		do_computation23(recursion + 1, recursion_depth);
	}
	else if(r == 24) {
		do_computation24(recursion + 1, recursion_depth);
	}
	else if(r == 25) {
		do_computation25(recursion + 1, recursion_depth);
	}
	else if(r == 26) {
		do_computation26(recursion + 1, recursion_depth);
	}
	else if(r == 27) {
		do_computation27(recursion + 1, recursion_depth);
	}
	else if(r == 28) {
		do_computation28(recursion + 1, recursion_depth);
	}
	else if(r == 29) {
		do_computation29(recursion + 1, recursion_depth);
	}
	else if(r == 30) {
		do_computation30(recursion + 1, recursion_depth);
	}
	else if(r == 31) {
		do_computation31(recursion + 1, recursion_depth);
	}
}
void do_computation14(int recursion, int recursion_depth) {
	if(recursion == recursion_depth) {
		return;
	}

	int r = recursion + 1;
	r = r << 27;
	r = r >> 27;
	if(r == 0) {
		do_computation(recursion + 1, recursion_depth);
	}
	else if(r == 1) {
		do_computation1(recursion + 1, recursion_depth);
	}
	else if(r == 2) {
		do_computation2(recursion + 1, recursion_depth);
	}
	else if(r == 3) {
		do_computation3(recursion + 1, recursion_depth);
	}
	else if(r == 4) {
		do_computation4(recursion + 1, recursion_depth);
	}
	else if(r == 5) {
		do_computation5(recursion + 1, recursion_depth);
	}
	else if(r == 6) {
		do_computation6(recursion + 1, recursion_depth);
	}
	else if(r == 7) {
		do_computation7(recursion + 1, recursion_depth);
	}
	else if(r == 8) {
		do_computation8(recursion + 1, recursion_depth);
	}
	else if(r == 9) {
		do_computation9(recursion + 1, recursion_depth);
	}
	else if(r == 10) {
		do_computation10(recursion + 1, recursion_depth);
	}
	else if(r == 11) {
		do_computation11(recursion + 1, recursion_depth);
	}
	else if(r == 12) {
		do_computation12(recursion + 1, recursion_depth);
	}
	else if(r == 13) {
		do_computation13(recursion + 1, recursion_depth);
	}
	else if(r == 14) {
		do_computation14(recursion + 1, recursion_depth);
	}
	else if(r == 15) {
		do_computation15(recursion + 1, recursion_depth);
	}
	else if(r == 16) {
		do_computation16(recursion + 1, recursion_depth);
	}
	else if(r == 17) {
		do_computation17(recursion + 1, recursion_depth);
	}
	else if(r == 18) {
		do_computation18(recursion + 1, recursion_depth);
	}
	else if(r == 19) {
		do_computation19(recursion + 1, recursion_depth);
	}
	else if(r == 20) {
		do_computation20(recursion + 1, recursion_depth);
	}
	else if(r == 21) {
		do_computation21(recursion + 1, recursion_depth);
	}
	else if(r == 22) {
		do_computation22(recursion + 1, recursion_depth);
	}
	else if(r == 23) {
		do_computation23(recursion + 1, recursion_depth);
	}
	else if(r == 24) {
		do_computation24(recursion + 1, recursion_depth);
	}
	else if(r == 25) {
		do_computation25(recursion + 1, recursion_depth);
	}
	else if(r == 26) {
		do_computation26(recursion + 1, recursion_depth);
	}
	else if(r == 27) {
		do_computation27(recursion + 1, recursion_depth);
	}
	else if(r == 28) {
		do_computation28(recursion + 1, recursion_depth);
	}
	else if(r == 29) {
		do_computation29(recursion + 1, recursion_depth);
	}
	else if(r == 30) {
		do_computation30(recursion + 1, recursion_depth);
	}
	else if(r == 31) {
		do_computation31(recursion + 1, recursion_depth);
	}
}
void do_computation15(int recursion, int recursion_depth) {
	if(recursion == recursion_depth) {
		return;
	}

	int r = recursion + 1;
	r = r << 27;
	r = r >> 27;
	if(r == 0) {
		do_computation(recursion + 1, recursion_depth);
	}
	else if(r == 1) {
		do_computation1(recursion + 1, recursion_depth);
	}
	else if(r == 2) {
		do_computation2(recursion + 1, recursion_depth);
	}
	else if(r == 3) {
		do_computation3(recursion + 1, recursion_depth);
	}
	else if(r == 4) {
		do_computation4(recursion + 1, recursion_depth);
	}
	else if(r == 5) {
		do_computation5(recursion + 1, recursion_depth);
	}
	else if(r == 6) {
		do_computation6(recursion + 1, recursion_depth);
	}
	else if(r == 7) {
		do_computation7(recursion + 1, recursion_depth);
	}
	else if(r == 8) {
		do_computation8(recursion + 1, recursion_depth);
	}
	else if(r == 9) {
		do_computation9(recursion + 1, recursion_depth);
	}
	else if(r == 10) {
		do_computation10(recursion + 1, recursion_depth);
	}
	else if(r == 11) {
		do_computation11(recursion + 1, recursion_depth);
	}
	else if(r == 12) {
		do_computation12(recursion + 1, recursion_depth);
	}
	else if(r == 13) {
		do_computation13(recursion + 1, recursion_depth);
	}
	else if(r == 14) {
		do_computation14(recursion + 1, recursion_depth);
	}
	else if(r == 15) {
		do_computation15(recursion + 1, recursion_depth);
	}
	else if(r == 16) {
		do_computation16(recursion + 1, recursion_depth);
	}
	else if(r == 17) {
		do_computation17(recursion + 1, recursion_depth);
	}
	else if(r == 18) {
		do_computation18(recursion + 1, recursion_depth);
	}
	else if(r == 19) {
		do_computation19(recursion + 1, recursion_depth);
	}
	else if(r == 20) {
		do_computation20(recursion + 1, recursion_depth);
	}
	else if(r == 21) {
		do_computation21(recursion + 1, recursion_depth);
	}
	else if(r == 22) {
		do_computation22(recursion + 1, recursion_depth);
	}
	else if(r == 23) {
		do_computation23(recursion + 1, recursion_depth);
	}
	else if(r == 24) {
		do_computation24(recursion + 1, recursion_depth);
	}
	else if(r == 25) {
		do_computation25(recursion + 1, recursion_depth);
	}
	else if(r == 26) {
		do_computation26(recursion + 1, recursion_depth);
	}
	else if(r == 27) {
		do_computation27(recursion + 1, recursion_depth);
	}
	else if(r == 28) {
		do_computation28(recursion + 1, recursion_depth);
	}
	else if(r == 29) {
		do_computation29(recursion + 1, recursion_depth);
	}
	else if(r == 30) {
		do_computation30(recursion + 1, recursion_depth);
	}
	else if(r == 31) {
		do_computation31(recursion + 1, recursion_depth);
	}
}
void do_computation16(int recursion, int recursion_depth) {
	if(recursion == recursion_depth) {
		return;
	}

	int r = recursion + 1;
	r = r << 27;
	r = r >> 27;
	if(r == 0) {
		do_computation(recursion + 1, recursion_depth);
	}
	else if(r == 1) {
		do_computation1(recursion + 1, recursion_depth);
	}
	else if(r == 2) {
		do_computation2(recursion + 1, recursion_depth);
	}
	else if(r == 3) {
		do_computation3(recursion + 1, recursion_depth);
	}
	else if(r == 4) {
		do_computation4(recursion + 1, recursion_depth);
	}
	else if(r == 5) {
		do_computation5(recursion + 1, recursion_depth);
	}
	else if(r == 6) {
		do_computation6(recursion + 1, recursion_depth);
	}
	else if(r == 7) {
		do_computation7(recursion + 1, recursion_depth);
	}
	else if(r == 8) {
		do_computation8(recursion + 1, recursion_depth);
	}
	else if(r == 9) {
		do_computation9(recursion + 1, recursion_depth);
	}
	else if(r == 10) {
		do_computation10(recursion + 1, recursion_depth);
	}
	else if(r == 11) {
		do_computation11(recursion + 1, recursion_depth);
	}
	else if(r == 12) {
		do_computation12(recursion + 1, recursion_depth);
	}
	else if(r == 13) {
		do_computation13(recursion + 1, recursion_depth);
	}
	else if(r == 14) {
		do_computation14(recursion + 1, recursion_depth);
	}
	else if(r == 15) {
		do_computation15(recursion + 1, recursion_depth);
	}
	else if(r == 16) {
		do_computation16(recursion + 1, recursion_depth);
	}
	else if(r == 17) {
		do_computation17(recursion + 1, recursion_depth);
	}
	else if(r == 18) {
		do_computation18(recursion + 1, recursion_depth);
	}
	else if(r == 19) {
		do_computation19(recursion + 1, recursion_depth);
	}
	else if(r == 20) {
		do_computation20(recursion + 1, recursion_depth);
	}
	else if(r == 21) {
		do_computation21(recursion + 1, recursion_depth);
	}
	else if(r == 22) {
		do_computation22(recursion + 1, recursion_depth);
	}
	else if(r == 23) {
		do_computation23(recursion + 1, recursion_depth);
	}
	else if(r == 24) {
		do_computation24(recursion + 1, recursion_depth);
	}
	else if(r == 25) {
		do_computation25(recursion + 1, recursion_depth);
	}
	else if(r == 26) {
		do_computation26(recursion + 1, recursion_depth);
	}
	else if(r == 27) {
		do_computation27(recursion + 1, recursion_depth);
	}
	else if(r == 28) {
		do_computation28(recursion + 1, recursion_depth);
	}
	else if(r == 29) {
		do_computation29(recursion + 1, recursion_depth);
	}
	else if(r == 30) {
		do_computation30(recursion + 1, recursion_depth);
	}
	else if(r == 31) {
		do_computation31(recursion + 1, recursion_depth);
	}
}
void do_computation17(int recursion, int recursion_depth) {
	if(recursion == recursion_depth) {
		return;
	}

	int r = recursion + 1;
	r = r << 27;
	r = r >> 27;
	if(r == 0) {
		do_computation(recursion + 1, recursion_depth);
	}
	else if(r == 1) {
		do_computation1(recursion + 1, recursion_depth);
	}
	else if(r == 2) {
		do_computation2(recursion + 1, recursion_depth);
	}
	else if(r == 3) {
		do_computation3(recursion + 1, recursion_depth);
	}
	else if(r == 4) {
		do_computation4(recursion + 1, recursion_depth);
	}
	else if(r == 5) {
		do_computation5(recursion + 1, recursion_depth);
	}
	else if(r == 6) {
		do_computation6(recursion + 1, recursion_depth);
	}
	else if(r == 7) {
		do_computation7(recursion + 1, recursion_depth);
	}
	else if(r == 8) {
		do_computation8(recursion + 1, recursion_depth);
	}
	else if(r == 9) {
		do_computation9(recursion + 1, recursion_depth);
	}
	else if(r == 10) {
		do_computation10(recursion + 1, recursion_depth);
	}
	else if(r == 11) {
		do_computation11(recursion + 1, recursion_depth);
	}
	else if(r == 12) {
		do_computation12(recursion + 1, recursion_depth);
	}
	else if(r == 13) {
		do_computation13(recursion + 1, recursion_depth);
	}
	else if(r == 14) {
		do_computation14(recursion + 1, recursion_depth);
	}
	else if(r == 15) {
		do_computation15(recursion + 1, recursion_depth);
	}
	else if(r == 16) {
		do_computation16(recursion + 1, recursion_depth);
	}
	else if(r == 17) {
		do_computation17(recursion + 1, recursion_depth);
	}
	else if(r == 18) {
		do_computation18(recursion + 1, recursion_depth);
	}
	else if(r == 19) {
		do_computation19(recursion + 1, recursion_depth);
	}
	else if(r == 20) {
		do_computation20(recursion + 1, recursion_depth);
	}
	else if(r == 21) {
		do_computation21(recursion + 1, recursion_depth);
	}
	else if(r == 22) {
		do_computation22(recursion + 1, recursion_depth);
	}
	else if(r == 23) {
		do_computation23(recursion + 1, recursion_depth);
	}
	else if(r == 24) {
		do_computation24(recursion + 1, recursion_depth);
	}
	else if(r == 25) {
		do_computation25(recursion + 1, recursion_depth);
	}
	else if(r == 26) {
		do_computation26(recursion + 1, recursion_depth);
	}
	else if(r == 27) {
		do_computation27(recursion + 1, recursion_depth);
	}
	else if(r == 28) {
		do_computation28(recursion + 1, recursion_depth);
	}
	else if(r == 29) {
		do_computation29(recursion + 1, recursion_depth);
	}
	else if(r == 30) {
		do_computation30(recursion + 1, recursion_depth);
	}
	else if(r == 31) {
		do_computation31(recursion + 1, recursion_depth);
	}
}
void do_computation18(int recursion, int recursion_depth) {
	if(recursion == recursion_depth) {
		return;
	}

	int r = recursion + 1;
	r = r << 27;
	r = r >> 27;
	if(r == 0) {
		do_computation(recursion + 1, recursion_depth);
	}
	else if(r == 1) {
		do_computation1(recursion + 1, recursion_depth);
	}
	else if(r == 2) {
		do_computation2(recursion + 1, recursion_depth);
	}
	else if(r == 3) {
		do_computation3(recursion + 1, recursion_depth);
	}
	else if(r == 4) {
		do_computation4(recursion + 1, recursion_depth);
	}
	else if(r == 5) {
		do_computation5(recursion + 1, recursion_depth);
	}
	else if(r == 6) {
		do_computation6(recursion + 1, recursion_depth);
	}
	else if(r == 7) {
		do_computation7(recursion + 1, recursion_depth);
	}
	else if(r == 8) {
		do_computation8(recursion + 1, recursion_depth);
	}
	else if(r == 9) {
		do_computation9(recursion + 1, recursion_depth);
	}
	else if(r == 10) {
		do_computation10(recursion + 1, recursion_depth);
	}
	else if(r == 11) {
		do_computation11(recursion + 1, recursion_depth);
	}
	else if(r == 12) {
		do_computation12(recursion + 1, recursion_depth);
	}
	else if(r == 13) {
		do_computation13(recursion + 1, recursion_depth);
	}
	else if(r == 14) {
		do_computation14(recursion + 1, recursion_depth);
	}
	else if(r == 15) {
		do_computation15(recursion + 1, recursion_depth);
	}
	else if(r == 16) {
		do_computation16(recursion + 1, recursion_depth);
	}
	else if(r == 17) {
		do_computation17(recursion + 1, recursion_depth);
	}
	else if(r == 18) {
		do_computation18(recursion + 1, recursion_depth);
	}
	else if(r == 19) {
		do_computation19(recursion + 1, recursion_depth);
	}
	else if(r == 20) {
		do_computation20(recursion + 1, recursion_depth);
	}
	else if(r == 21) {
		do_computation21(recursion + 1, recursion_depth);
	}
	else if(r == 22) {
		do_computation22(recursion + 1, recursion_depth);
	}
	else if(r == 23) {
		do_computation23(recursion + 1, recursion_depth);
	}
	else if(r == 24) {
		do_computation24(recursion + 1, recursion_depth);
	}
	else if(r == 25) {
		do_computation25(recursion + 1, recursion_depth);
	}
	else if(r == 26) {
		do_computation26(recursion + 1, recursion_depth);
	}
	else if(r == 27) {
		do_computation27(recursion + 1, recursion_depth);
	}
	else if(r == 28) {
		do_computation28(recursion + 1, recursion_depth);
	}
	else if(r == 29) {
		do_computation29(recursion + 1, recursion_depth);
	}
	else if(r == 30) {
		do_computation30(recursion + 1, recursion_depth);
	}
	else if(r == 31) {
		do_computation31(recursion + 1, recursion_depth);
	}
}
void do_computation19(int recursion, int recursion_depth) {
	if(recursion == recursion_depth) {
		return;
	}

	int r = recursion + 1;
	r = r << 27;
	r = r >> 27;
	if(r == 0) {
		do_computation(recursion + 1, recursion_depth);
	}
	else if(r == 1) {
		do_computation1(recursion + 1, recursion_depth);
	}
	else if(r == 2) {
		do_computation2(recursion + 1, recursion_depth);
	}
	else if(r == 3) {
		do_computation3(recursion + 1, recursion_depth);
	}
	else if(r == 4) {
		do_computation4(recursion + 1, recursion_depth);
	}
	else if(r == 5) {
		do_computation5(recursion + 1, recursion_depth);
	}
	else if(r == 6) {
		do_computation6(recursion + 1, recursion_depth);
	}
	else if(r == 7) {
		do_computation7(recursion + 1, recursion_depth);
	}
	else if(r == 8) {
		do_computation8(recursion + 1, recursion_depth);
	}
	else if(r == 9) {
		do_computation9(recursion + 1, recursion_depth);
	}
	else if(r == 10) {
		do_computation10(recursion + 1, recursion_depth);
	}
	else if(r == 11) {
		do_computation11(recursion + 1, recursion_depth);
	}
	else if(r == 12) {
		do_computation12(recursion + 1, recursion_depth);
	}
	else if(r == 13) {
		do_computation13(recursion + 1, recursion_depth);
	}
	else if(r == 14) {
		do_computation14(recursion + 1, recursion_depth);
	}
	else if(r == 15) {
		do_computation15(recursion + 1, recursion_depth);
	}
	else if(r == 16) {
		do_computation16(recursion + 1, recursion_depth);
	}
	else if(r == 17) {
		do_computation17(recursion + 1, recursion_depth);
	}
	else if(r == 18) {
		do_computation18(recursion + 1, recursion_depth);
	}
	else if(r == 19) {
		do_computation19(recursion + 1, recursion_depth);
	}
	else if(r == 20) {
		do_computation20(recursion + 1, recursion_depth);
	}
	else if(r == 21) {
		do_computation21(recursion + 1, recursion_depth);
	}
	else if(r == 22) {
		do_computation22(recursion + 1, recursion_depth);
	}
	else if(r == 23) {
		do_computation23(recursion + 1, recursion_depth);
	}
	else if(r == 24) {
		do_computation24(recursion + 1, recursion_depth);
	}
	else if(r == 25) {
		do_computation25(recursion + 1, recursion_depth);
	}
	else if(r == 26) {
		do_computation26(recursion + 1, recursion_depth);
	}
	else if(r == 27) {
		do_computation27(recursion + 1, recursion_depth);
	}
	else if(r == 28) {
		do_computation28(recursion + 1, recursion_depth);
	}
	else if(r == 29) {
		do_computation29(recursion + 1, recursion_depth);
	}
	else if(r == 30) {
		do_computation30(recursion + 1, recursion_depth);
	}
	else if(r == 31) {
		do_computation31(recursion + 1, recursion_depth);
	}
}
void do_computation20(int recursion, int recursion_depth) {
	if(recursion == recursion_depth) {
		return;
	}

	int r = recursion + 1;
	r = r << 27;
	r = r >> 27;
	if(r == 0) {
		do_computation(recursion + 1, recursion_depth);
	}
	else if(r == 1) {
		do_computation1(recursion + 1, recursion_depth);
	}
	else if(r == 2) {
		do_computation2(recursion + 1, recursion_depth);
	}
	else if(r == 3) {
		do_computation3(recursion + 1, recursion_depth);
	}
	else if(r == 4) {
		do_computation4(recursion + 1, recursion_depth);
	}
	else if(r == 5) {
		do_computation5(recursion + 1, recursion_depth);
	}
	else if(r == 6) {
		do_computation6(recursion + 1, recursion_depth);
	}
	else if(r == 7) {
		do_computation7(recursion + 1, recursion_depth);
	}
	else if(r == 8) {
		do_computation8(recursion + 1, recursion_depth);
	}
	else if(r == 9) {
		do_computation9(recursion + 1, recursion_depth);
	}
	else if(r == 10) {
		do_computation10(recursion + 1, recursion_depth);
	}
	else if(r == 11) {
		do_computation11(recursion + 1, recursion_depth);
	}
	else if(r == 12) {
		do_computation12(recursion + 1, recursion_depth);
	}
	else if(r == 13) {
		do_computation13(recursion + 1, recursion_depth);
	}
	else if(r == 14) {
		do_computation14(recursion + 1, recursion_depth);
	}
	else if(r == 15) {
		do_computation15(recursion + 1, recursion_depth);
	}
	else if(r == 16) {
		do_computation16(recursion + 1, recursion_depth);
	}
	else if(r == 17) {
		do_computation17(recursion + 1, recursion_depth);
	}
	else if(r == 18) {
		do_computation18(recursion + 1, recursion_depth);
	}
	else if(r == 19) {
		do_computation19(recursion + 1, recursion_depth);
	}
	else if(r == 20) {
		do_computation20(recursion + 1, recursion_depth);
	}
	else if(r == 21) {
		do_computation21(recursion + 1, recursion_depth);
	}
	else if(r == 22) {
		do_computation22(recursion + 1, recursion_depth);
	}
	else if(r == 23) {
		do_computation23(recursion + 1, recursion_depth);
	}
	else if(r == 24) {
		do_computation24(recursion + 1, recursion_depth);
	}
	else if(r == 25) {
		do_computation25(recursion + 1, recursion_depth);
	}
	else if(r == 26) {
		do_computation26(recursion + 1, recursion_depth);
	}
	else if(r == 27) {
		do_computation27(recursion + 1, recursion_depth);
	}
	else if(r == 28) {
		do_computation28(recursion + 1, recursion_depth);
	}
	else if(r == 29) {
		do_computation29(recursion + 1, recursion_depth);
	}
	else if(r == 30) {
		do_computation30(recursion + 1, recursion_depth);
	}
	else if(r == 31) {
		do_computation31(recursion + 1, recursion_depth);
	}
}
void do_computation21(int recursion, int recursion_depth) {
	if(recursion == recursion_depth) {
		return;
	}

	int r = recursion + 1;
	r = r << 27;
	r = r >> 27;
	if(r == 0) {
		do_computation(recursion + 1, recursion_depth);
	}
	else if(r == 1) {
		do_computation1(recursion + 1, recursion_depth);
	}
	else if(r == 2) {
		do_computation2(recursion + 1, recursion_depth);
	}
	else if(r == 3) {
		do_computation3(recursion + 1, recursion_depth);
	}
	else if(r == 4) {
		do_computation4(recursion + 1, recursion_depth);
	}
	else if(r == 5) {
		do_computation5(recursion + 1, recursion_depth);
	}
	else if(r == 6) {
		do_computation6(recursion + 1, recursion_depth);
	}
	else if(r == 7) {
		do_computation7(recursion + 1, recursion_depth);
	}
	else if(r == 8) {
		do_computation8(recursion + 1, recursion_depth);
	}
	else if(r == 9) {
		do_computation9(recursion + 1, recursion_depth);
	}
	else if(r == 10) {
		do_computation10(recursion + 1, recursion_depth);
	}
	else if(r == 11) {
		do_computation11(recursion + 1, recursion_depth);
	}
	else if(r == 12) {
		do_computation12(recursion + 1, recursion_depth);
	}
	else if(r == 13) {
		do_computation13(recursion + 1, recursion_depth);
	}
	else if(r == 14) {
		do_computation14(recursion + 1, recursion_depth);
	}
	else if(r == 15) {
		do_computation15(recursion + 1, recursion_depth);
	}
	else if(r == 16) {
		do_computation16(recursion + 1, recursion_depth);
	}
	else if(r == 17) {
		do_computation17(recursion + 1, recursion_depth);
	}
	else if(r == 18) {
		do_computation18(recursion + 1, recursion_depth);
	}
	else if(r == 19) {
		do_computation19(recursion + 1, recursion_depth);
	}
	else if(r == 20) {
		do_computation20(recursion + 1, recursion_depth);
	}
	else if(r == 21) {
		do_computation21(recursion + 1, recursion_depth);
	}
	else if(r == 22) {
		do_computation22(recursion + 1, recursion_depth);
	}
	else if(r == 23) {
		do_computation23(recursion + 1, recursion_depth);
	}
	else if(r == 24) {
		do_computation24(recursion + 1, recursion_depth);
	}
	else if(r == 25) {
		do_computation25(recursion + 1, recursion_depth);
	}
	else if(r == 26) {
		do_computation26(recursion + 1, recursion_depth);
	}
	else if(r == 27) {
		do_computation27(recursion + 1, recursion_depth);
	}
	else if(r == 28) {
		do_computation28(recursion + 1, recursion_depth);
	}
	else if(r == 29) {
		do_computation29(recursion + 1, recursion_depth);
	}
	else if(r == 30) {
		do_computation30(recursion + 1, recursion_depth);
	}
	else if(r == 31) {
		do_computation31(recursion + 1, recursion_depth);
	}
}
void do_computation22(int recursion, int recursion_depth) {
	if(recursion == recursion_depth) {
		return;
	}

	int r = recursion + 1;
	r = r << 27;
	r = r >> 27;
	if(r == 0) {
		do_computation(recursion + 1, recursion_depth);
	}
	else if(r == 1) {
		do_computation1(recursion + 1, recursion_depth);
	}
	else if(r == 2) {
		do_computation2(recursion + 1, recursion_depth);
	}
	else if(r == 3) {
		do_computation3(recursion + 1, recursion_depth);
	}
	else if(r == 4) {
		do_computation4(recursion + 1, recursion_depth);
	}
	else if(r == 5) {
		do_computation5(recursion + 1, recursion_depth);
	}
	else if(r == 6) {
		do_computation6(recursion + 1, recursion_depth);
	}
	else if(r == 7) {
		do_computation7(recursion + 1, recursion_depth);
	}
	else if(r == 8) {
		do_computation8(recursion + 1, recursion_depth);
	}
	else if(r == 9) {
		do_computation9(recursion + 1, recursion_depth);
	}
	else if(r == 10) {
		do_computation10(recursion + 1, recursion_depth);
	}
	else if(r == 11) {
		do_computation11(recursion + 1, recursion_depth);
	}
	else if(r == 12) {
		do_computation12(recursion + 1, recursion_depth);
	}
	else if(r == 13) {
		do_computation13(recursion + 1, recursion_depth);
	}
	else if(r == 14) {
		do_computation14(recursion + 1, recursion_depth);
	}
	else if(r == 15) {
		do_computation15(recursion + 1, recursion_depth);
	}
	else if(r == 16) {
		do_computation16(recursion + 1, recursion_depth);
	}
	else if(r == 17) {
		do_computation17(recursion + 1, recursion_depth);
	}
	else if(r == 18) {
		do_computation18(recursion + 1, recursion_depth);
	}
	else if(r == 19) {
		do_computation19(recursion + 1, recursion_depth);
	}
	else if(r == 20) {
		do_computation20(recursion + 1, recursion_depth);
	}
	else if(r == 21) {
		do_computation21(recursion + 1, recursion_depth);
	}
	else if(r == 22) {
		do_computation22(recursion + 1, recursion_depth);
	}
	else if(r == 23) {
		do_computation23(recursion + 1, recursion_depth);
	}
	else if(r == 24) {
		do_computation24(recursion + 1, recursion_depth);
	}
	else if(r == 25) {
		do_computation25(recursion + 1, recursion_depth);
	}
	else if(r == 26) {
		do_computation26(recursion + 1, recursion_depth);
	}
	else if(r == 27) {
		do_computation27(recursion + 1, recursion_depth);
	}
	else if(r == 28) {
		do_computation28(recursion + 1, recursion_depth);
	}
	else if(r == 29) {
		do_computation29(recursion + 1, recursion_depth);
	}
	else if(r == 30) {
		do_computation30(recursion + 1, recursion_depth);
	}
	else if(r == 31) {
		do_computation31(recursion + 1, recursion_depth);
	}
}
void do_computation23(int recursion, int recursion_depth) {
	if(recursion == recursion_depth) {
		return;
	}

	int r = recursion + 1;
	r = r << 27;
	r = r >> 27;
	if(r == 0) {
		do_computation(recursion + 1, recursion_depth);
	}
	else if(r == 1) {
		do_computation1(recursion + 1, recursion_depth);
	}
	else if(r == 2) {
		do_computation2(recursion + 1, recursion_depth);
	}
	else if(r == 3) {
		do_computation3(recursion + 1, recursion_depth);
	}
	else if(r == 4) {
		do_computation4(recursion + 1, recursion_depth);
	}
	else if(r == 5) {
		do_computation5(recursion + 1, recursion_depth);
	}
	else if(r == 6) {
		do_computation6(recursion + 1, recursion_depth);
	}
	else if(r == 7) {
		do_computation7(recursion + 1, recursion_depth);
	}
	else if(r == 8) {
		do_computation8(recursion + 1, recursion_depth);
	}
	else if(r == 9) {
		do_computation9(recursion + 1, recursion_depth);
	}
	else if(r == 10) {
		do_computation10(recursion + 1, recursion_depth);
	}
	else if(r == 11) {
		do_computation11(recursion + 1, recursion_depth);
	}
	else if(r == 12) {
		do_computation12(recursion + 1, recursion_depth);
	}
	else if(r == 13) {
		do_computation13(recursion + 1, recursion_depth);
	}
	else if(r == 14) {
		do_computation14(recursion + 1, recursion_depth);
	}
	else if(r == 15) {
		do_computation15(recursion + 1, recursion_depth);
	}
	else if(r == 16) {
		do_computation16(recursion + 1, recursion_depth);
	}
	else if(r == 17) {
		do_computation17(recursion + 1, recursion_depth);
	}
	else if(r == 18) {
		do_computation18(recursion + 1, recursion_depth);
	}
	else if(r == 19) {
		do_computation19(recursion + 1, recursion_depth);
	}
	else if(r == 20) {
		do_computation20(recursion + 1, recursion_depth);
	}
	else if(r == 21) {
		do_computation21(recursion + 1, recursion_depth);
	}
	else if(r == 22) {
		do_computation22(recursion + 1, recursion_depth);
	}
	else if(r == 23) {
		do_computation23(recursion + 1, recursion_depth);
	}
	else if(r == 24) {
		do_computation24(recursion + 1, recursion_depth);
	}
	else if(r == 25) {
		do_computation25(recursion + 1, recursion_depth);
	}
	else if(r == 26) {
		do_computation26(recursion + 1, recursion_depth);
	}
	else if(r == 27) {
		do_computation27(recursion + 1, recursion_depth);
	}
	else if(r == 28) {
		do_computation28(recursion + 1, recursion_depth);
	}
	else if(r == 29) {
		do_computation29(recursion + 1, recursion_depth);
	}
	else if(r == 30) {
		do_computation30(recursion + 1, recursion_depth);
	}
	else if(r == 31) {
		do_computation31(recursion + 1, recursion_depth);
	}
}
void do_computation24(int recursion, int recursion_depth) {
	if(recursion == recursion_depth) {
		return;
	}

	int r = recursion + 1;
	r = r << 27;
	r = r >> 27;
	if(r == 0) {
		do_computation(recursion + 1, recursion_depth);
	}
	else if(r == 1) {
		do_computation1(recursion + 1, recursion_depth);
	}
	else if(r == 2) {
		do_computation2(recursion + 1, recursion_depth);
	}
	else if(r == 3) {
		do_computation3(recursion + 1, recursion_depth);
	}
	else if(r == 4) {
		do_computation4(recursion + 1, recursion_depth);
	}
	else if(r == 5) {
		do_computation5(recursion + 1, recursion_depth);
	}
	else if(r == 6) {
		do_computation6(recursion + 1, recursion_depth);
	}
	else if(r == 7) {
		do_computation7(recursion + 1, recursion_depth);
	}
	else if(r == 8) {
		do_computation8(recursion + 1, recursion_depth);
	}
	else if(r == 9) {
		do_computation9(recursion + 1, recursion_depth);
	}
	else if(r == 10) {
		do_computation10(recursion + 1, recursion_depth);
	}
	else if(r == 11) {
		do_computation11(recursion + 1, recursion_depth);
	}
	else if(r == 12) {
		do_computation12(recursion + 1, recursion_depth);
	}
	else if(r == 13) {
		do_computation13(recursion + 1, recursion_depth);
	}
	else if(r == 14) {
		do_computation14(recursion + 1, recursion_depth);
	}
	else if(r == 15) {
		do_computation15(recursion + 1, recursion_depth);
	}
	else if(r == 16) {
		do_computation16(recursion + 1, recursion_depth);
	}
	else if(r == 17) {
		do_computation17(recursion + 1, recursion_depth);
	}
	else if(r == 18) {
		do_computation18(recursion + 1, recursion_depth);
	}
	else if(r == 19) {
		do_computation19(recursion + 1, recursion_depth);
	}
	else if(r == 20) {
		do_computation20(recursion + 1, recursion_depth);
	}
	else if(r == 21) {
		do_computation21(recursion + 1, recursion_depth);
	}
	else if(r == 22) {
		do_computation22(recursion + 1, recursion_depth);
	}
	else if(r == 23) {
		do_computation23(recursion + 1, recursion_depth);
	}
	else if(r == 24) {
		do_computation24(recursion + 1, recursion_depth);
	}
	else if(r == 25) {
		do_computation25(recursion + 1, recursion_depth);
	}
	else if(r == 26) {
		do_computation26(recursion + 1, recursion_depth);
	}
	else if(r == 27) {
		do_computation27(recursion + 1, recursion_depth);
	}
	else if(r == 28) {
		do_computation28(recursion + 1, recursion_depth);
	}
	else if(r == 29) {
		do_computation29(recursion + 1, recursion_depth);
	}
	else if(r == 30) {
		do_computation30(recursion + 1, recursion_depth);
	}
	else if(r == 31) {
		do_computation31(recursion + 1, recursion_depth);
	}
}
void do_computation25(int recursion, int recursion_depth) {
	if(recursion == recursion_depth) {
		return;
	}

	int r = recursion + 1;
	r = r << 27;
	r = r >> 27;
	if(r == 0) {
		do_computation(recursion + 1, recursion_depth);
	}
	else if(r == 1) {
		do_computation1(recursion + 1, recursion_depth);
	}
	else if(r == 2) {
		do_computation2(recursion + 1, recursion_depth);
	}
	else if(r == 3) {
		do_computation3(recursion + 1, recursion_depth);
	}
	else if(r == 4) {
		do_computation4(recursion + 1, recursion_depth);
	}
	else if(r == 5) {
		do_computation5(recursion + 1, recursion_depth);
	}
	else if(r == 6) {
		do_computation6(recursion + 1, recursion_depth);
	}
	else if(r == 7) {
		do_computation7(recursion + 1, recursion_depth);
	}
	else if(r == 8) {
		do_computation8(recursion + 1, recursion_depth);
	}
	else if(r == 9) {
		do_computation9(recursion + 1, recursion_depth);
	}
	else if(r == 10) {
		do_computation10(recursion + 1, recursion_depth);
	}
	else if(r == 11) {
		do_computation11(recursion + 1, recursion_depth);
	}
	else if(r == 12) {
		do_computation12(recursion + 1, recursion_depth);
	}
	else if(r == 13) {
		do_computation13(recursion + 1, recursion_depth);
	}
	else if(r == 14) {
		do_computation14(recursion + 1, recursion_depth);
	}
	else if(r == 15) {
		do_computation15(recursion + 1, recursion_depth);
	}
	else if(r == 16) {
		do_computation16(recursion + 1, recursion_depth);
	}
	else if(r == 17) {
		do_computation17(recursion + 1, recursion_depth);
	}
	else if(r == 18) {
		do_computation18(recursion + 1, recursion_depth);
	}
	else if(r == 19) {
		do_computation19(recursion + 1, recursion_depth);
	}
	else if(r == 20) {
		do_computation20(recursion + 1, recursion_depth);
	}
	else if(r == 21) {
		do_computation21(recursion + 1, recursion_depth);
	}
	else if(r == 22) {
		do_computation22(recursion + 1, recursion_depth);
	}
	else if(r == 23) {
		do_computation23(recursion + 1, recursion_depth);
	}
	else if(r == 24) {
		do_computation24(recursion + 1, recursion_depth);
	}
	else if(r == 25) {
		do_computation25(recursion + 1, recursion_depth);
	}
	else if(r == 26) {
		do_computation26(recursion + 1, recursion_depth);
	}
	else if(r == 27) {
		do_computation27(recursion + 1, recursion_depth);
	}
	else if(r == 28) {
		do_computation28(recursion + 1, recursion_depth);
	}
	else if(r == 29) {
		do_computation29(recursion + 1, recursion_depth);
	}
	else if(r == 30) {
		do_computation30(recursion + 1, recursion_depth);
	}
	else if(r == 31) {
		do_computation31(recursion + 1, recursion_depth);
	}
}
void do_computation26(int recursion, int recursion_depth) {
	if(recursion == recursion_depth) {
		return;
	}

	int r = recursion + 1;
	r = r << 27;
	r = r >> 27;
	if(r == 0) {
		do_computation(recursion + 1, recursion_depth);
	}
	else if(r == 1) {
		do_computation1(recursion + 1, recursion_depth);
	}
	else if(r == 2) {
		do_computation2(recursion + 1, recursion_depth);
	}
	else if(r == 3) {
		do_computation3(recursion + 1, recursion_depth);
	}
	else if(r == 4) {
		do_computation4(recursion + 1, recursion_depth);
	}
	else if(r == 5) {
		do_computation5(recursion + 1, recursion_depth);
	}
	else if(r == 6) {
		do_computation6(recursion + 1, recursion_depth);
	}
	else if(r == 7) {
		do_computation7(recursion + 1, recursion_depth);
	}
	else if(r == 8) {
		do_computation8(recursion + 1, recursion_depth);
	}
	else if(r == 9) {
		do_computation9(recursion + 1, recursion_depth);
	}
	else if(r == 10) {
		do_computation10(recursion + 1, recursion_depth);
	}
	else if(r == 11) {
		do_computation11(recursion + 1, recursion_depth);
	}
	else if(r == 12) {
		do_computation12(recursion + 1, recursion_depth);
	}
	else if(r == 13) {
		do_computation13(recursion + 1, recursion_depth);
	}
	else if(r == 14) {
		do_computation14(recursion + 1, recursion_depth);
	}
	else if(r == 15) {
		do_computation15(recursion + 1, recursion_depth);
	}
	else if(r == 16) {
		do_computation16(recursion + 1, recursion_depth);
	}
	else if(r == 17) {
		do_computation17(recursion + 1, recursion_depth);
	}
	else if(r == 18) {
		do_computation18(recursion + 1, recursion_depth);
	}
	else if(r == 19) {
		do_computation19(recursion + 1, recursion_depth);
	}
	else if(r == 20) {
		do_computation20(recursion + 1, recursion_depth);
	}
	else if(r == 21) {
		do_computation21(recursion + 1, recursion_depth);
	}
	else if(r == 22) {
		do_computation22(recursion + 1, recursion_depth);
	}
	else if(r == 23) {
		do_computation23(recursion + 1, recursion_depth);
	}
	else if(r == 24) {
		do_computation24(recursion + 1, recursion_depth);
	}
	else if(r == 25) {
		do_computation25(recursion + 1, recursion_depth);
	}
	else if(r == 26) {
		do_computation26(recursion + 1, recursion_depth);
	}
	else if(r == 27) {
		do_computation27(recursion + 1, recursion_depth);
	}
	else if(r == 28) {
		do_computation28(recursion + 1, recursion_depth);
	}
	else if(r == 29) {
		do_computation29(recursion + 1, recursion_depth);
	}
	else if(r == 30) {
		do_computation30(recursion + 1, recursion_depth);
	}
	else if(r == 31) {
		do_computation31(recursion + 1, recursion_depth);
	}
}
void do_computation27(int recursion, int recursion_depth) {
	if(recursion == recursion_depth) {
		return;
	}

	int r = recursion + 1;
	r = r << 27;
	r = r >> 27;
	if(r == 0) {
		do_computation(recursion + 1, recursion_depth);
	}
	else if(r == 1) {
		do_computation1(recursion + 1, recursion_depth);
	}
	else if(r == 2) {
		do_computation2(recursion + 1, recursion_depth);
	}
	else if(r == 3) {
		do_computation3(recursion + 1, recursion_depth);
	}
	else if(r == 4) {
		do_computation4(recursion + 1, recursion_depth);
	}
	else if(r == 5) {
		do_computation5(recursion + 1, recursion_depth);
	}
	else if(r == 6) {
		do_computation6(recursion + 1, recursion_depth);
	}
	else if(r == 7) {
		do_computation7(recursion + 1, recursion_depth);
	}
	else if(r == 8) {
		do_computation8(recursion + 1, recursion_depth);
	}
	else if(r == 9) {
		do_computation9(recursion + 1, recursion_depth);
	}
	else if(r == 10) {
		do_computation10(recursion + 1, recursion_depth);
	}
	else if(r == 11) {
		do_computation11(recursion + 1, recursion_depth);
	}
	else if(r == 12) {
		do_computation12(recursion + 1, recursion_depth);
	}
	else if(r == 13) {
		do_computation13(recursion + 1, recursion_depth);
	}
	else if(r == 14) {
		do_computation14(recursion + 1, recursion_depth);
	}
	else if(r == 15) {
		do_computation15(recursion + 1, recursion_depth);
	}
	else if(r == 16) {
		do_computation16(recursion + 1, recursion_depth);
	}
	else if(r == 17) {
		do_computation17(recursion + 1, recursion_depth);
	}
	else if(r == 18) {
		do_computation18(recursion + 1, recursion_depth);
	}
	else if(r == 19) {
		do_computation19(recursion + 1, recursion_depth);
	}
	else if(r == 20) {
		do_computation20(recursion + 1, recursion_depth);
	}
	else if(r == 21) {
		do_computation21(recursion + 1, recursion_depth);
	}
	else if(r == 22) {
		do_computation22(recursion + 1, recursion_depth);
	}
	else if(r == 23) {
		do_computation23(recursion + 1, recursion_depth);
	}
	else if(r == 24) {
		do_computation24(recursion + 1, recursion_depth);
	}
	else if(r == 25) {
		do_computation25(recursion + 1, recursion_depth);
	}
	else if(r == 26) {
		do_computation26(recursion + 1, recursion_depth);
	}
	else if(r == 27) {
		do_computation27(recursion + 1, recursion_depth);
	}
	else if(r == 28) {
		do_computation28(recursion + 1, recursion_depth);
	}
	else if(r == 29) {
		do_computation29(recursion + 1, recursion_depth);
	}
	else if(r == 30) {
		do_computation30(recursion + 1, recursion_depth);
	}
	else if(r == 31) {
		do_computation31(recursion + 1, recursion_depth);
	}
}
void do_computation28(int recursion, int recursion_depth) {
	if(recursion == recursion_depth) {
		return;
	}

	int r = recursion + 1;
	r = r << 27;
	r = r >> 27;
	if(r == 0) {
		do_computation(recursion + 1, recursion_depth);
	}
	else if(r == 1) {
		do_computation1(recursion + 1, recursion_depth);
	}
	else if(r == 2) {
		do_computation2(recursion + 1, recursion_depth);
	}
	else if(r == 3) {
		do_computation3(recursion + 1, recursion_depth);
	}
	else if(r == 4) {
		do_computation4(recursion + 1, recursion_depth);
	}
	else if(r == 5) {
		do_computation5(recursion + 1, recursion_depth);
	}
	else if(r == 6) {
		do_computation6(recursion + 1, recursion_depth);
	}
	else if(r == 7) {
		do_computation7(recursion + 1, recursion_depth);
	}
	else if(r == 8) {
		do_computation8(recursion + 1, recursion_depth);
	}
	else if(r == 9) {
		do_computation9(recursion + 1, recursion_depth);
	}
	else if(r == 10) {
		do_computation10(recursion + 1, recursion_depth);
	}
	else if(r == 11) {
		do_computation11(recursion + 1, recursion_depth);
	}
	else if(r == 12) {
		do_computation12(recursion + 1, recursion_depth);
	}
	else if(r == 13) {
		do_computation13(recursion + 1, recursion_depth);
	}
	else if(r == 14) {
		do_computation14(recursion + 1, recursion_depth);
	}
	else if(r == 15) {
		do_computation15(recursion + 1, recursion_depth);
	}
	else if(r == 16) {
		do_computation16(recursion + 1, recursion_depth);
	}
	else if(r == 17) {
		do_computation17(recursion + 1, recursion_depth);
	}
	else if(r == 18) {
		do_computation18(recursion + 1, recursion_depth);
	}
	else if(r == 19) {
		do_computation19(recursion + 1, recursion_depth);
	}
	else if(r == 20) {
		do_computation20(recursion + 1, recursion_depth);
	}
	else if(r == 21) {
		do_computation21(recursion + 1, recursion_depth);
	}
	else if(r == 22) {
		do_computation22(recursion + 1, recursion_depth);
	}
	else if(r == 23) {
		do_computation23(recursion + 1, recursion_depth);
	}
	else if(r == 24) {
		do_computation24(recursion + 1, recursion_depth);
	}
	else if(r == 25) {
		do_computation25(recursion + 1, recursion_depth);
	}
	else if(r == 26) {
		do_computation26(recursion + 1, recursion_depth);
	}
	else if(r == 27) {
		do_computation27(recursion + 1, recursion_depth);
	}
	else if(r == 28) {
		do_computation28(recursion + 1, recursion_depth);
	}
	else if(r == 29) {
		do_computation29(recursion + 1, recursion_depth);
	}
	else if(r == 30) {
		do_computation30(recursion + 1, recursion_depth);
	}
	else if(r == 31) {
		do_computation31(recursion + 1, recursion_depth);
	}
}
void do_computation29(int recursion, int recursion_depth) {
	if(recursion == recursion_depth) {
		return;
	}

	int r = recursion + 1;
	r = r << 27;
	r = r >> 27;
	if(r == 0) {
		do_computation(recursion + 1, recursion_depth);
	}
	else if(r == 1) {
		do_computation1(recursion + 1, recursion_depth);
	}
	else if(r == 2) {
		do_computation2(recursion + 1, recursion_depth);
	}
	else if(r == 3) {
		do_computation3(recursion + 1, recursion_depth);
	}
	else if(r == 4) {
		do_computation4(recursion + 1, recursion_depth);
	}
	else if(r == 5) {
		do_computation5(recursion + 1, recursion_depth);
	}
	else if(r == 6) {
		do_computation6(recursion + 1, recursion_depth);
	}
	else if(r == 7) {
		do_computation7(recursion + 1, recursion_depth);
	}
	else if(r == 8) {
		do_computation8(recursion + 1, recursion_depth);
	}
	else if(r == 9) {
		do_computation9(recursion + 1, recursion_depth);
	}
	else if(r == 10) {
		do_computation10(recursion + 1, recursion_depth);
	}
	else if(r == 11) {
		do_computation11(recursion + 1, recursion_depth);
	}
	else if(r == 12) {
		do_computation12(recursion + 1, recursion_depth);
	}
	else if(r == 13) {
		do_computation13(recursion + 1, recursion_depth);
	}
	else if(r == 14) {
		do_computation14(recursion + 1, recursion_depth);
	}
	else if(r == 15) {
		do_computation15(recursion + 1, recursion_depth);
	}
	else if(r == 16) {
		do_computation16(recursion + 1, recursion_depth);
	}
	else if(r == 17) {
		do_computation17(recursion + 1, recursion_depth);
	}
	else if(r == 18) {
		do_computation18(recursion + 1, recursion_depth);
	}
	else if(r == 19) {
		do_computation19(recursion + 1, recursion_depth);
	}
	else if(r == 20) {
		do_computation20(recursion + 1, recursion_depth);
	}
	else if(r == 21) {
		do_computation21(recursion + 1, recursion_depth);
	}
	else if(r == 22) {
		do_computation22(recursion + 1, recursion_depth);
	}
	else if(r == 23) {
		do_computation23(recursion + 1, recursion_depth);
	}
	else if(r == 24) {
		do_computation24(recursion + 1, recursion_depth);
	}
	else if(r == 25) {
		do_computation25(recursion + 1, recursion_depth);
	}
	else if(r == 26) {
		do_computation26(recursion + 1, recursion_depth);
	}
	else if(r == 27) {
		do_computation27(recursion + 1, recursion_depth);
	}
	else if(r == 28) {
		do_computation28(recursion + 1, recursion_depth);
	}
	else if(r == 29) {
		do_computation29(recursion + 1, recursion_depth);
	}
	else if(r == 30) {
		do_computation30(recursion + 1, recursion_depth);
	}
	else if(r == 31) {
		do_computation31(recursion + 1, recursion_depth);
	}
}
void do_computation30(int recursion, int recursion_depth) {
	if(recursion == recursion_depth) {
		return;
	}

	int r = recursion + 1;
	r = r << 27;
	r = r >> 27;
	if(r == 0) {
		do_computation(recursion + 1, recursion_depth);
	}
	else if(r == 1) {
		do_computation1(recursion + 1, recursion_depth);
	}
	else if(r == 2) {
		do_computation2(recursion + 1, recursion_depth);
	}
	else if(r == 3) {
		do_computation3(recursion + 1, recursion_depth);
	}
	else if(r == 4) {
		do_computation4(recursion + 1, recursion_depth);
	}
	else if(r == 5) {
		do_computation5(recursion + 1, recursion_depth);
	}
	else if(r == 6) {
		do_computation6(recursion + 1, recursion_depth);
	}
	else if(r == 7) {
		do_computation7(recursion + 1, recursion_depth);
	}
	else if(r == 8) {
		do_computation8(recursion + 1, recursion_depth);
	}
	else if(r == 9) {
		do_computation9(recursion + 1, recursion_depth);
	}
	else if(r == 10) {
		do_computation10(recursion + 1, recursion_depth);
	}
	else if(r == 11) {
		do_computation11(recursion + 1, recursion_depth);
	}
	else if(r == 12) {
		do_computation12(recursion + 1, recursion_depth);
	}
	else if(r == 13) {
		do_computation13(recursion + 1, recursion_depth);
	}
	else if(r == 14) {
		do_computation14(recursion + 1, recursion_depth);
	}
	else if(r == 15) {
		do_computation15(recursion + 1, recursion_depth);
	}
	else if(r == 16) {
		do_computation16(recursion + 1, recursion_depth);
	}
	else if(r == 17) {
		do_computation17(recursion + 1, recursion_depth);
	}
	else if(r == 18) {
		do_computation18(recursion + 1, recursion_depth);
	}
	else if(r == 19) {
		do_computation19(recursion + 1, recursion_depth);
	}
	else if(r == 20) {
		do_computation20(recursion + 1, recursion_depth);
	}
	else if(r == 21) {
		do_computation21(recursion + 1, recursion_depth);
	}
	else if(r == 22) {
		do_computation22(recursion + 1, recursion_depth);
	}
	else if(r == 23) {
		do_computation23(recursion + 1, recursion_depth);
	}
	else if(r == 24) {
		do_computation24(recursion + 1, recursion_depth);
	}
	else if(r == 25) {
		do_computation25(recursion + 1, recursion_depth);
	}
	else if(r == 26) {
		do_computation26(recursion + 1, recursion_depth);
	}
	else if(r == 27) {
		do_computation27(recursion + 1, recursion_depth);
	}
	else if(r == 28) {
		do_computation28(recursion + 1, recursion_depth);
	}
	else if(r == 29) {
		do_computation29(recursion + 1, recursion_depth);
	}
	else if(r == 30) {
		do_computation30(recursion + 1, recursion_depth);
	}
	else if(r == 31) {
		do_computation31(recursion + 1, recursion_depth);
	}
}
void do_computation31(int recursion, int recursion_depth) {
	if(recursion == recursion_depth) {
		return;
	}

	int r = recursion + 1;
	r = r << 27;
	r = r >> 27;
	if(r == 0) {
		do_computation(recursion + 1, recursion_depth);
	}
	else if(r == 1) {
		do_computation1(recursion + 1, recursion_depth);
	}
	else if(r == 2) {
		do_computation2(recursion + 1, recursion_depth);
	}
	else if(r == 3) {
		do_computation3(recursion + 1, recursion_depth);
	}
	else if(r == 4) {
		do_computation4(recursion + 1, recursion_depth);
	}
	else if(r == 5) {
		do_computation5(recursion + 1, recursion_depth);
	}
	else if(r == 6) {
		do_computation6(recursion + 1, recursion_depth);
	}
	else if(r == 7) {
		do_computation7(recursion + 1, recursion_depth);
	}
	else if(r == 8) {
		do_computation8(recursion + 1, recursion_depth);
	}
	else if(r == 9) {
		do_computation9(recursion + 1, recursion_depth);
	}
	else if(r == 10) {
		do_computation10(recursion + 1, recursion_depth);
	}
	else if(r == 11) {
		do_computation11(recursion + 1, recursion_depth);
	}
	else if(r == 12) {
		do_computation12(recursion + 1, recursion_depth);
	}
	else if(r == 13) {
		do_computation13(recursion + 1, recursion_depth);
	}
	else if(r == 14) {
		do_computation14(recursion + 1, recursion_depth);
	}
	else if(r == 15) {
		do_computation15(recursion + 1, recursion_depth);
	}
	else if(r == 16) {
		do_computation16(recursion + 1, recursion_depth);
	}
	else if(r == 17) {
		do_computation17(recursion + 1, recursion_depth);
	}
	else if(r == 18) {
		do_computation18(recursion + 1, recursion_depth);
	}
	else if(r == 19) {
		do_computation19(recursion + 1, recursion_depth);
	}
	else if(r == 20) {
		do_computation20(recursion + 1, recursion_depth);
	}
	else if(r == 21) {
		do_computation21(recursion + 1, recursion_depth);
	}
	else if(r == 22) {
		do_computation22(recursion + 1, recursion_depth);
	}
	else if(r == 23) {
		do_computation23(recursion + 1, recursion_depth);
	}
	else if(r == 24) {
		do_computation24(recursion + 1, recursion_depth);
	}
	else if(r == 25) {
		do_computation25(recursion + 1, recursion_depth);
	}
	else if(r == 26) {
		do_computation26(recursion + 1, recursion_depth);
	}
	else if(r == 27) {
		do_computation27(recursion + 1, recursion_depth);
	}
	else if(r == 28) {
		do_computation28(recursion + 1, recursion_depth);
	}
	else if(r == 29) {
		do_computation29(recursion + 1, recursion_depth);
	}
	else if(r == 30) {
		do_computation30(recursion + 1, recursion_depth);
	}
	else if(r == 31) {
		do_computation31(recursion + 1, recursion_depth);
	}
}

int main () {
	clock_t before_computation, after_computation, average;
	clock_t clock_latencies[OUTER_TIMES_TO_REPEAT], results[MAX_RECURSION_DEPTH - MIN_RECURSION_DEPTH];

	printf("Initial clock: %lu\n", clock());

	int i, j, k;
	// repeat experiment for each recursion depth
	for(i = MIN_RECURSION_DEPTH; i < MAX_RECURSION_DEPTH; ++i) {
		// repeat many times to take the average
		for(j = 0; j < OUTER_TIMES_TO_REPEAT; ++j) {
			before_computation = clock();
			// repeat many times to measure a significant number of cicles
			for(k = 0; k < INNER_TIMES_TO_REPEAT; ++k) {
				do_computation(0, i);
			}

			after_computation = clock();
			clock_latencies[j] = after_computation - before_computation;
		}
		
		average = 0;
		for(j = 0; j < OUTER_TIMES_TO_REPEAT; ++j) {
			average += clock_latencies[j];
		}
		average /= OUTER_TIMES_TO_REPEAT;
		results[i - MIN_RECURSION_DEPTH] = average;
	}

	printf("Final clock: %lu\n", clock());

	char *filename = "result-ras32.csv";
	FILE *result_file = fopen(filename, "w");
	if (result_file == NULL)
	{
	    printf("Error opening file!\n");
	    return 0;
	}

	for(i = 0; i < MAX_RECURSION_DEPTH - MIN_RECURSION_DEPTH; ++i) {
		fprintf(result_file, "%d\t%lu\n", i, results[i]);
	}

	fclose(result_file);
	printf("Results written in file \"%s\"\n", filename);

	return 0;
}
