import numpy as np
import matplotlib.pyplot as plt

def read_datafile(file_name):
    data = np.genfromtxt(file_name, delimiter='\t', names=['x','y'])
    return data


data = read_datafile('result-ras32.csv')

fig = plt.figure()
ax1 = fig.add_subplot(111)

ax1.set_title("RAS 32")
ax1.set_xlabel('recursion functions')
ax1.set_ylabel('cicles')
ax1.plot(data['x'], data['y'], color='r', label='the data')

image_file_name = 'result-ras32.png'
plt.savefig(image_file_name)
print 'Plot saved in \"{0}\"'.format(image_file_name)